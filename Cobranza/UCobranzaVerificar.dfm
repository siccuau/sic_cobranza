﻿object CobranzaVerificar: TCobranzaVerificar
  Left = 0
  Top = 0
  Caption = 'Verificar Contrase'#241'a'
  ClientHeight = 93
  ClientWidth = 264
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 80
    Top = 0
    Width = 90
    Height = 18
    Caption = 'Contrase'#241'a:'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -15
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object StatusBar1: TStatusBar
    Left = 0
    Top = 80
    Width = 264
    Height = 13
    Panels = <
      item
        Width = 50
      end>
    ExplicitLeft = -794
    ExplicitTop = 229
    ExplicitWidth = 1321
  end
  object txtContraseña: TEdit
    Left = 8
    Top = 24
    Width = 241
    Height = 21
    PasswordChar = '*'
    TabOrder = 1
  end
  object btnEntrar: TButton
    Left = 95
    Top = 51
    Width = 75
    Height = 25
    Caption = 'Entrar'
    TabOrder = 2
    OnClick = btnEntrarClick
  end
  object Conexion: TFDConnection
    Params.Strings = (
      'User_Name=sysdba'
      'Password=masterkey'
      'Database=D:\Microsip pruebas\AD2019.FDB'
      'Protocol=TCPIP'
      'Server=192.168.0.50'
      'DriverID=FB')
    LoginPrompt = False
    Left = 32
    Top = 136
  end
end
