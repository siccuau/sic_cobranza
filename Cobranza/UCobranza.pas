unit UCobranza;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, FireDAC.Stan.Intf, FireDAC.Stan.Option,
  FireDAC.Stan.Error, FireDAC.UI.Intf, FireDAC.Phys.Intf, FireDAC.Stan.Def,
  FireDAC.Stan.Pool, FireDAC.Stan.Async, FireDAC.Phys, FireDAC.Phys.FB,
  FireDAC.Phys.FBDef, FireDAC.VCLUI.Wait, Data.DB, FireDAC.Comp.Client,
  Vcl.ComCtrls, FireDAC.Stan.Param, FireDAC.DatS, FireDAC.DApt.Intf,
  FireDAC.DApt, Vcl.Grids, Vcl.DBGrids, Vcl.StdCtrls, FireDAC.Comp.DataSet,
  Vcl.ExtCtrls, Vcl.DBCtrls, Data.Bind.EngExt, Vcl.Bind.DBEngExt,
  Data.Bind.Components, System.Rtti, System.Bindings.Outputs, Vcl.Bind.Editors,
  Data.Bind.DBScope, frxClass, frxDBSet, System.Character, Datasnap.DBClient,
  Datasnap.DSConnect,UCobranzaDevolver,UCobranzaAdmin,UCobranzaAdminCambiar,UCobranzaVerificar;

type
  TCobranza = class(TForm)
    Conexion: TFDConnection;
    StatusBar1: TStatusBar;
    qryCargosPendientes: TFDQuery;
    dsCargosPendientes: TDataSource;
    qryCargosPendientesFECHA: TDateField;
    qryCargosPendientesFOLIO: TStringField;
    qryCargosPendientesNOMBRE_CLIENTE: TStringField;
    qryCargosPendientesENTREGADO: TStringField;
    qryCargosPendientesDIAS_ATRASO: TLargeintField;
    qryCobradores: TFDQuery;
    BindSourceDB1: TBindSourceDB;
    BindingsList1: TBindingsList;
    qryCobranza: TFDQuery;
    rptSalida: TfrxReport;
    dsSalida: TfrxDBDataset;
    qryCargosPendientesDOCTO_CC_ID: TIntegerField;
    qryCobranzaCARGO_CC_ID: TIntegerField;
    qryCobranzaFOLIO: TStringField;
    qryCobranzaFECHA: TDateField;
    qryCobranzaCOBRADOR_ID: TIntegerField;
    qryCobranzaFECHAHORA_SALIDA: TSQLTimeStampField;
    qryCobranzaTIPO_SALIDA: TStringField;
    qryCobranzaFECHAHORA_ENTREGA: TSQLTimeStampField;
    qryCobranzaTIPO_ENTREGA: TStringField;
    qryCobranzaPROX_VISITA: TDateField;
    qryCobranzaACLARACIONES: TStringField;
    qryCobranzaENTREGA_DEFINITIVA: TStringField;
    qryCargosPendientesCOBRADOR: TStringField;
    qryCobradoresRecepcion: TFDQuery;
    BindSourceDB2: TBindSourceDB;
    qrycargosCobrador: TFDQuery;
    dsCargosCobrador: TDataSource;
    qryCargosPendientesTIPO_SALIDA: TStringField;
    qryCargosPendientesTIPO_SALIDA_STR: TStringField;
    rptRecepcion: TfrxReport;
    dsRecepcion: TfrxDBDataset;
    qryReporteRecepcion: TFDQuery;
    qryReportesFecha: TFDQuery;
    BindSourceDB3: TBindSourceDB;
    qryReportesFechaNOMBRE: TStringField;
    qryCobradoresRecepcionNOMBRE: TStringField;
    qryCobradoresRecepcionCOBRADOR_ID: TIntegerField;
    qryCobradoresRecepcionPOLITICA_COMIS_COB_ID: TIntegerField;
    qryCobradoresRecepcionES_PREDET: TStringField;
    qryCobradoresRecepcionUSUARIO_CREADOR: TStringField;
    qryCobradoresRecepcionFECHA_HORA_CREACION: TSQLTimeStampField;
    qryCobradoresRecepcionUSUARIO_AUT_CREACION: TStringField;
    qryCobradoresRecepcionUSUARIO_ULT_MODIF: TStringField;
    qryCobradoresRecepcionFECHA_HORA_ULT_MODIF: TSQLTimeStampField;
    qryCobradoresRecepcionUSUARIO_AUT_MODIF: TStringField;
    qryFecha: TFDQuery;
    dsFecha: TfrxDBDataset;
    rptFecha: TfrxReport;
    qryReportesFechaCOBRADOR_ID: TIntegerField;
    qryReportesFechaPOLITICA_COMIS_COB_ID: TIntegerField;
    qryReportesFechaES_PREDET: TStringField;
    qryReportesFechaUSUARIO_CREADOR: TStringField;
    qryReportesFechaFECHA_HORA_CREACION: TSQLTimeStampField;
    qryReportesFechaUSUARIO_AUT_CREACION: TStringField;
    qryReportesFechaUSUARIO_ULT_MODIF: TStringField;
    qryReportesFechaFECHA_HORA_ULT_MODIF: TSQLTimeStampField;
    qryReportesFechaUSUARIO_AUT_MODIF: TStringField;
    qryCobradoresCOBRADOR_ID: TIntegerField;
    qryCobradoresNOMBRE: TStringField;
    qryCobradoresPOLITICA_COMIS_COB_ID: TIntegerField;
    qryCobradoresES_PREDET: TStringField;
    qryCobradoresUSUARIO_CREADOR: TStringField;
    qryCobradoresFECHA_HORA_CREACION: TSQLTimeStampField;
    qryCobradoresUSUARIO_AUT_CREACION: TStringField;
    qryCobradoresUSUARIO_ULT_MODIF: TStringField;
    qryCobradoresFECHA_HORA_ULT_MODIF: TSQLTimeStampField;
    qryCobradoresUSUARIO_AUT_MODIF: TStringField;
    qryFechaFECHA: TDateField;
    qryFechaFOLIO: TStringField;
    qryFechaNOMBRE_CLIENTE: TStringField;
    qryFechaCLIENTE_ID: TIntegerField;
    qryFechaSALDO_CARGO: TBCDField;
    qryFechaTIPO_SALIDA: TStringField;
    qryFechaFECHAHORA_SALIDA: TSQLTimeStampField;
    qryFechaDOCTO_CC_ID: TIntegerField;
    qryFechaCOBRADOR_ID: TIntegerField;
    qryFechaCOBRADOR: TStringField;
    qryFechaFECHAHORA_ENTREGA: TSQLTimeStampField;
    qryFechaTIPO_ENTREGA: TStringField;
    qryFechaACLARACIONES: TStringField;
    qryFechaPROX_VISITA: TDateField;
    qryFechaENTREGA_DEFINITIVA: TStringField;
    qryFechaFOLIO_PAGADO: TStringField;
    qryFolio: TFDQuery;
    rptFolio: TfrxReport;
    dsFolio: TfrxDBDataset;
    PageControl1: TPageControl;
    tsSalida: TTabSheet;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    lblNumCargos: TLabel;
    Label4: TLabel;
    dtpFin: TDateTimePicker;
    dtpInicio: TDateTimePicker;
    btnBuscar: TButton;
    grdCargosSalida: TDBGrid;
    btnGenerarSalida: TButton;
    rgOrden: TRadioGroup;
    cbAll: TCheckBox;
    txtbusquedaSalida: TEdit;
    CheckBox1: TCheckBox;
    ComboBox1: TComboBox;
    pnlDesc1: TPanel;
    cbcobradores: TComboBox;
    tsRecepcion: TTabSheet;
    Label5: TLabel;
    cbCobradoresRecepcion: TComboBox;
    grdCargoscobrador: TDBGrid;
    btnGuardarRecepcion: TButton;
    cbCargosCobrador: TCheckBox;
    DateTimePicker1: TDateTimePicker;
    ComboBox3: TComboBox;
    ChkAllRecepcion: TCheckBox;
    Panel1: TPanel;
    chkLiquidados: TCheckBox;
    tsReportes: TTabSheet;
    GroupBox1: TGroupBox;
    Label8: TLabel;
    Label6: TLabel;
    Label7: TLabel;
    DateTimePicker2: TDateTimePicker;
    DateTimePicker3: TDateTimePicker;
    ComboBox2: TComboBox;
    Button1: TButton;
    RadioGroup1: TRadioGroup;
    CheckBox2: TCheckBox;
    GroupBox2: TGroupBox;
    txtFolio: TEdit;
    StaticText1: TStaticText;
    Button2: TButton;
    LinkListControlToField1: TLinkListControlToField;
    LinkListControlToField2: TLinkListControlToField;
    LinkListControlToField3: TLinkListControlToField;
    chkRemisiones: TCheckBox;
    Button3: TButton;
    grdRemisiones: TDBGrid;
    Label9: TLabel;
    DSProviderConnection1: TDSProviderConnection;
    DSProviderConnection2: TDSProviderConnection;
    dsRemisiones: TDataSource;
    CheckBox3: TCheckBox;
    lblNumRemisiones: TLabel;
    cbAllRemisiones: TCheckBox;
    Label10: TLabel;
    CheckBox4: TCheckBox;
    grdRemisionesRecepcion: TDBGrid;
    CheckBox5: TCheckBox;
    dsRemisionesRecepcion: TDataSource;
    qryRemisionesRecepcion: TFDQuery;
    CheckBox6: TCheckBox;
    dsRemisiones1: TfrxDBDataset;
    dsRemisiones2: TDataSource;
    qryRemisiones1: TFDQuery;
    dsRemisionesRecepcion1: TfrxDBDataset;
    qryRemisionesRecepcionENTREGADO: TStringField;
    qryRemisionesRecepcionFOLIO: TStringField;
    qryRemisionesRecepcionFECHA: TSQLTimeStampField;
    qryRemisionesRecepcionCLIENTE: TStringField;
    qryRemisionesRecepcionCOBRADOR: TStringField;
    qryRemisionesRecepcionFOLIO_PAGADO: TStringField;
    qryRemisionesRecepcionCLIENTE_ID: TIntegerField;
    qryRemisionesRecepcionENTREGA_DEFINITIVA: TStringField;
    qryRemisionesRecepcionIMPORTE: TBCDField;
    qryRemisiones: TFDQuery;
    qryRemisionesENTREGADO: TStringField;
    qryRemisionesFECHA: TDateField;
    qryRemisionesFOLIO: TStringField;
    qryRemisionesNOMBRE_CLIENTE: TStringField;
    qryRemisionesDIAS_ATRASO: TLargeintField;
    qryRemisionesIMPORTE: TBCDField;
    qryRemisionesCOBRADOR: TStringField;
    qryRemisionesDOCTO_VE_ID: TIntegerField;
    qryRemisionesRecepcion1: TFDQuery;
    Button4: TButton;
    Button5: TButton;
    TabSheet1: TTabSheet;
    btnPass: TButton;
    btnNuevaContraseña: TButton;
    qryClientes: TFDQuery;
    dsClientes: TDataSource;
    qryClientesCLIENTE_ID: TIntegerField;
    qryClientesNOMBRE: TStringField;
    cbCliente: TComboBox;
    BindSourceDB4: TBindSourceDB;
    LinkListControlToField4: TLinkListControlToField;
    cbVendedor: TComboBox;
    qryVendedores: TFDQuery;
    qryVendedoresVENDEDOR_ID: TIntegerField;
    qryVendedoresNOMBRE: TStringField;
    BindSourceDB5: TBindSourceDB;
    LinkListControlToField5: TLinkListControlToField;
    GroupBox3: TGroupBox;
    txtFolioSalida: TEdit;
    StaticText2: TStaticText;
    Button6: TButton;
    Button7: TButton;
    rptFolioSalida: TfrxReport;
    dsFolioSalida: TfrxDBDataset;
    qryFolioSalida: TFDQuery;
    qryFolioSalidaFOLIO: TStringField;
    qryFolioSalidaTIPO_SALIDA: TStringField;
    qryFolioSalidaNOMBRE: TStringField;
    qryFolioSalidaFECHA_1: TDateField;
    qryFolioSalidaFOLIO_PAGADO: TStringField;
    qryFolioSalidaTIPO_ENTREGA: TStringField;
    qryFolioSalidaACLARACIONES: TStringField;
    qryFolioSalidaPROX_VISITA: TDateField;
    qryFolioSalidaFOLIO_G: TStringField;
    btnTransito: TButton;
    qryTransito: TFDQuery;
    dsTransito: TfrxDBDataset;
    rptTransito: TfrxReport;
    qryFolioCARGO_CC_ID: TIntegerField;
    qryFolioFOLIO: TStringField;
    qryFolioFECHA: TDateField;
    qryFolioCOBRADOR_ID: TIntegerField;
    qryFolioFECHAHORA_SALIDA: TSQLTimeStampField;
    qryFolioTIPO_SALIDA: TStringField;
    qryFolioFECHAHORA_ENTREGA: TSQLTimeStampField;
    qryFolioTIPO_ENTREGA: TStringField;
    qryFolioPROX_VISITA: TDateField;
    qryFolioACLARACIONES: TStringField;
    qryFolioENTREGA_DEFINITIVA: TStringField;
    qryFolioFOLIO_PAGADO: TStringField;
    qryFolioCOBRADOR_ID_1: TIntegerField;
    qryFolioNOMBRE: TStringField;
    qryFolioPOLITICA_COMIS_COB_ID: TIntegerField;
    qryFolioES_PREDET: TStringField;
    qryFolioUSUARIO_CREADOR: TStringField;
    qryFolioFECHA_HORA_CREACION: TSQLTimeStampField;
    qryFolioUSUARIO_AUT_CREACION: TStringField;
    qryFolioUSUARIO_ULT_MODIF: TStringField;
    qryFolioFECHA_HORA_ULT_MODIF: TSQLTimeStampField;
    qryFolioUSUARIO_AUT_MODIF: TStringField;
    qryFolioDOCTO_VE_ID: TIntegerField;
    qryFolioTIPO_DOCTO: TStringField;
    qryFolioSUBTIPO_DOCTO: TStringField;
    qryFolioFOLIO_1: TStringField;
    qryFolioFECHA_1: TDateField;
    qryFolioHORA: TTimeField;
    qryFolioCLAVE_CLIENTE: TStringField;
    qryFolioCLIENTE_ID: TIntegerField;
    qryFolioDIR_CLI_ID: TIntegerField;
    qryFolioDIR_CONSIG_ID: TIntegerField;
    qryFolioALMACEN_ID: TIntegerField;
    qryFolioLUGAR_EXPEDICION_ID: TIntegerField;
    qryFolioMONEDA_ID: TIntegerField;
    qryFolioTIPO_CAMBIO: TFMTBCDField;
    qryFolioTIPO_DSCTO: TStringField;
    qryFolioDSCTO_PCTJE: TFMTBCDField;
    qryFolioDSCTO_IMPORTE: TBCDField;
    qryFolioESTATUS: TStringField;
    qryFolioAPLICADO: TStringField;
    qryFolioFECHA_VIGENCIA_ENTREGA: TDateField;
    qryFolioORDEN_COMPRA: TStringField;
    qryFolioFECHA_ORDEN_COMPRA: TDateField;
    qryFolioFOLIO_RECIBO_MERCANCIA: TStringField;
    qryFolioFECHA_RECIBO_MERCANCIA: TDateField;
    qryFolioDESCRIPCION: TStringField;
    qryFolioIMPORTE_NETO: TBCDField;
    qryFolioFLETES: TBCDField;
    qryFolioOTROS_CARGOS: TBCDField;
    qryFolioTOTAL_IMPUESTOS: TBCDField;
    qryFolioTOTAL_RETENCIONES: TBCDField;
    qryFolioTOTAL_ANTICIPOS: TBCDField;
    qryFolioPESO_EMBARQUE: TBCDField;
    qryFolioFORMA_EMITIDA: TStringField;
    qryFolioCONTABILIZADO: TStringField;
    qryFolioACREDITAR_CXC: TStringField;
    qryFolioSISTEMA_ORIGEN: TStringField;
    qryFolioCOND_PAGO_ID: TIntegerField;
    qryFolioFECHA_DSCTO_PPAG: TDateField;
    qryFolioPCTJE_DSCTO_PPAG: TFMTBCDField;
    qryFolioVENDEDOR_ID: TIntegerField;
    qryFolioPCTJE_COMIS: TFMTBCDField;
    qryFolioVIA_EMBARQUE_ID: TIntegerField;
    qryFolioIMPORTE_COBRO: TBCDField;
    qryFolioDESCRIPCION_COBRO: TStringField;
    qryFolioIMPUESTO_SUSTITUIDO_ID: TIntegerField;
    qryFolioIMPUESTO_SUSTITUTO_ID: TIntegerField;
    qryFolioUSUARIO_CREADOR_1: TStringField;
    qryFolioES_CFD: TStringField;
    qryFolioMODALIDAD_FACTURACION: TStringField;
    qryFolioENVIADO: TStringField;
    qryFolioFECHA_HORA_ENVIO: TSQLTimeStampField;
    qryFolioEMAIL_ENVIO: TStringField;
    qryFolioCFD_ENVIO_ESPECIAL: TStringField;
    qryFolioUSO_CFDI: TStringField;
    qryFolioMETODO_PAGO_SAT: TStringField;
    qryFolioCFDI_CERTIFICADO: TStringField;
    qryFolioCFDI_FACT_DEVUELTA_ID: TIntegerField;
    qryFolioFECHA_HORA_CREACION_1: TSQLTimeStampField;
    qryFolioUSUARIO_ULT_MODIF_1: TStringField;
    qryFolioUSUARIO_AUT_CREACION_1: TStringField;
    qryFolioFECHA_HORA_ULT_MODIF_1: TSQLTimeStampField;
    qryFolioCARGAR_SUN: TStringField;
    qryFolioUSUARIO_AUT_MODIF_1: TStringField;
    qryFolioUSUARIO_CANCELACION: TStringField;
    qryFolioFECHA_HORA_CANCELACION: TSQLTimeStampField;
    qryFolioUSUARIO_AUT_CANCELACION: TStringField;
    qryFolioCOBRADOR_ID_2: TIntegerField;
    qrySalida: TFDQuery;
    qrySalidaRecepcion: TFDQuery;
    qryRemisionesRecepcionCARGO_CC_ID: TIntegerField;
    qryRemisiones1COBRADOR: TStringField;
    qryRemisiones1FOLIO: TStringField;
    qryRemisiones1FECHA: TDateField;
    qryRemisiones1FECHAHORA_SALIDA: TSQLTimeStampField;
    qryRemisiones1TIPO_SALIDA: TStringField;
    qryRemisiones1SALDO_CARGO: TBCDField;
    qryRemisiones1FECHA_G: TDateField;
    qryRemisiones1IMPORTE: TBCDField;
    qryRemisiones1NOMBRE: TStringField;
    qrySalidaRecepcionFECHAHORA_ENTREGA: TSQLTimeStampField;
    qrySalidaRecepcionFOLIO_PAGADO: TStringField;
    qrySalidaRecepcionTIPO_ENTREGA: TStringField;
    qrySalidaRecepcionACLARACIONES: TStringField;
    qrySalidaRecepcionPROX_VISITA: TDateField;
    qrySalidaRecepcionNOMBRE: TStringField;
    qrySalidaRecepcionFOLIO: TStringField;
    qrySalidaRecepcionFECHA: TDateField;
    qrySalidaRecepcionFECHAHORA_SALIDA: TSQLTimeStampField;
    qrySalidaRecepcionTIPO_SALIDA: TStringField;
    qrySalidaRecepcionFECHA_G: TDateField;
    qrySalidaRecepcionCOBRADOR: TStringField;
    qryRemisionesRecepcion1COBRADOR: TStringField;
    qryRemisionesRecepcion1IMPORTE: TBCDField;
    qryRemisionesRecepcion1NOMBRE: TStringField;
    qryRemisionesRecepcion1FOLIO: TStringField;
    qryRemisionesRecepcion1FECHA: TDateField;
    qryRemisionesRecepcion1FECHAHORA_SALIDA: TSQLTimeStampField;
    qryRemisionesRecepcion1TIPO_SALIDA: TStringField;
    qryRemisionesRecepcion1SALDO_CARGO: TBCDField;
    qryRemisionesRecepcion1FECHA_G: TDateField;
    qrySalidaCOBRADOR: TStringField;
    qrySalidaTIPO_SALIDA: TStringField;
    qrySalidaFOLIO: TStringField;
    qrySalidaFECHA: TDateField;
    qrySalidaFECHAHORA_SALIDA: TSQLTimeStampField;
    qrySalidaIMPORTE: TBCDField;
    qrycargosCobradorFOLIO: TStringField;
    qrycargosCobradorFECHA: TDateField;
    qrycargosCobradorNOMBRE_CLIENTE: TStringField;
    qrycargosCobradorTIPO_SALIDA: TStringField;
    qrycargosCobradorFECHAHORA_SALIDA: TSQLTimeStampField;
    qrycargosCobradorCOBRADOR_ID: TIntegerField;
    qrycargosCobradorCOBRADOR: TStringField;
    qrycargosCobradorFECHAHORA_ENTREGA: TSQLTimeStampField;
    qrycargosCobradorTIPO_ENTREGA: TStringField;
    qrycargosCobradorACLARACIONES: TStringField;
    qrycargosCobradorPROX_VISITA: TDateField;
    qrycargosCobradorENTREGA_DEFINITIVA: TStringField;
    qrycargosCobradorFOLIO_PAGADO: TStringField;
    qrycargosCobradorSALDO_CARGO: TBCDField;
    qrycargosCobradorCLIENTE_ID: TIntegerField;
    qrycargosCobradorDOCTO_CC_ID: TIntegerField;
    qrycargosCobradorTIPO_ENTREGA_STR: TStringField;
    qrySalidaFECHA_G: TDateField;
    ClientDataSet1: TClientDataSet;
    qryCargosPendientesCLIENTE_ID: TIntegerField;
    qryCargosPendientesIMPORTE_ORIG_CARGO: TBCDField;
    qryCargosPendientesSALDO_CARGO: TBCDField;
    cbZona: TComboBox;
    qryZona: TFDQuery;
    qryZonaZONA_CLIENTE_ID: TIntegerField;
    qryZonaNOMBRE: TStringField;
    qryZonaCUENTA_CXC: TStringField;
    qryZonaCUENTA_ANTICIPOS: TStringField;
    qryZonaES_PREDET: TStringField;
    qryZonaOCULTO: TStringField;
    qryZonaUSUARIO_CREADOR: TStringField;
    qryZonaFECHA_HORA_CREACION: TSQLTimeStampField;
    qryZonaUSUARIO_AUT_CREACION: TStringField;
    qryZonaUSUARIO_ULT_MODIF: TStringField;
    qryZonaFECHA_HORA_ULT_MODIF: TSQLTimeStampField;
    qryZonaUSUARIO_AUT_MODIF: TStringField;
    BindSourceDB6: TBindSourceDB;
    LinkListControlToField6: TLinkListControlToField;
    qrySalidaNOMBRE_CLIENTE: TStringField;
    qrycargosCobradorSALDO_CARGO1: TBCDField;
    cbConcepto: TDBLookupComboBox;
    Label11: TLabel;
    qryConceptosCobro: TFDQuery;
    dsConceptosCobro: TDataSource;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure rgOrdenClick(Sender: TObject);
    procedure cargadatos;
    procedure consultareporte;
    procedure consultafactura;
    procedure consultasalida;
    procedure btnBuscarClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure qryCargosPendientesAfterOpen(DataSet: TDataSet);
    procedure grdCargosSalidaDrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure CheckBox1Click(Sender: TObject);
    procedure cbAllClick(Sender: TObject);
    procedure qryCargosPendientesAfterRefresh(DataSet: TDataSet);
    procedure txtbusquedaSalidaKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure ComboBox1Change(Sender: TObject);
    procedure qryCargosPendientesCalcFields(DataSet: TDataSet);
    procedure btnGenerarSalidaClick(Sender: TObject);
    procedure GenerarSalida;
    procedure GenerarSalidaRemisiones;
    procedure ReporteSalida;
    procedure grdCargoscobradorDrawColumnCell(Sender: TObject;
      const Rect: TRect; DataCol: Integer; Column: TColumn;
      State: TGridDrawState);
    procedure cbCargosCobradorClick(Sender: TObject);
    procedure btnGuardarRecepcionClick(Sender: TObject);
    procedure ComboBox3Change(Sender: TObject);
    procedure DateTimePicker1Change(Sender: TObject);
    procedure qrycargosCobradorCalcFields(DataSet: TDataSet);
    procedure PageControl1Enter(Sender: TObject);
    procedure cbCobradoresRecepcionChange(Sender: TObject);
    procedure ChkAllRecepcionClick(Sender: TObject);
    procedure chkLiquidadosClick(Sender: TObject);
    procedure PageControl1Change(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure CheckBox2Click(Sender: TObject);
    procedure Button6Click(Sender: TObject);
    procedure chkRemisionesClick(Sender: TObject);
    procedure Button3Click(Sender: TObject);
    procedure grdRemisionesDrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure CheckBox3Click(Sender: TObject);
    procedure cbAllRemisionesClick(Sender: TObject);
    procedure CheckBox5Click(Sender: TObject);
    procedure grdRemisionesRecepcionDrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure CheckBox4Click(Sender: TObject);
    procedure CheckBox6Click(Sender: TObject);
    procedure Button4Click(Sender: TObject);
    procedure Button5Click(Sender: TObject);
    procedure TabSheet1Show(Sender: TObject);
    procedure btnNuevaContraseñaClick(Sender: TObject);
    procedure btnPassClick(Sender: TObject);
    procedure cbClienteChange(Sender: TObject);
    procedure cbVendedorChange(Sender: TObject);
    procedure btnTransitoClick(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure Button7Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure grdCargosSalidaKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure grdCargoscobradorKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
      procedure FocusCell(
   const DBGrid : TDBGrid;
   const column : integer) ; overload;
   function GetCellRect(ACol, ARow : Integer) : TRect;
 procedure DBGrid1MouseUp(Sender: TObject; Button: TMouseButton; Shift:
    TShiftState; X, Y: Integer);
procedure SimulateClick(ACol, ARow : Integer);
    procedure cbVendedorKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure cbConceptoCloseUp(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    orden,folio_str : string;
  end;

var
  Cobranza: TCobranza;

implementation

{$R *.dfm}
 function GetSerie(folio : String) : String;
var
  i:integer;
begin
  Result := '';
  for I := 1 to (folio.Length) do
  begin
    if (IsLetter(folio[i]))
    then Result := Result+folio[i]
    else exit;
  end;
end;

function GetNumFolio(folio : String) : String;
var
  i:integer;
begin
  Result := '';
  for I := 1 to (folio.Length) do
  begin
    if (IsNumber(folio[i]))
    then Result := Result+folio[i];
  end;
end;

procedure TCobranza.GenerarSalida;
var
folio_nuevo,serie:string;
concepto_id,consecutivo:integer;
begin
qryCobranza.Open;
   //GENERAR FOLIO NUEVO
   try
  serie := GetSerie(Conexion.ExecSQLScalar('select SIG_FOLIO from conceptos_cc where upper(nombre) = ''SALIDA DE COBRANZA'''));
  consecutivo := strtoint(GetnumFolio(Conexion.ExecSQLScalar('select SIG_FOLIO from conceptos_cc where upper(nombre) = ''SALIDA DE COBRANZA''')));
  concepto_id := Conexion.ExecSQLScalar('select concepto_cc_id from conceptos_cc where upper(nombre) = ''SALIDA DE COBRANZA''');
   except
   ShowMessage('Falta concepto ''Salida de Cobranza'' en Cuentas por Cobrar con naturaleza ''Cargo'' y folio automatico: ''S1''');
   end;
  folio_str := serie + Format('%.*d',[9-length(serie),(consecutivo)]);
  folio_nuevo := serie + Format('%.*d',[9-length(serie),(consecutivo+1)]);
  Conexion.ExecSQL('update conceptos_cc set sig_folio=:sig where concepto_cc_id=:concepto_id',[folio_nuevo,concepto_id]);
  //RECORRE TODOS LOS CARGOS PENDIENTES
  qryCargosPendientes.First;
  while not qryCargosPendientes.Eof do
  begin
    //VALIDA SI ESTA SELECCIONADO
    if qryCargosPendientesENTREGADO.Value = 'S' then
    begin
      with qryCobranza do
      begin
        //INSERTAR EN LA TABLA DE SIC_COBRANZA_DET
        Conexion.ExecSQL('insert into sic_cobranza_det(id,cargo_cc_id,fecha,folio) values (-1,:cargo_cc_id,current_date,:folio)',[qryCargosPendientesDOCTO_CC_ID.Value,folio_str]);
        //REVISA SI NO ESTA REGISTRADA UNA SALIDA DE ESE DOCUMENTO EN LA TABLA DE COBRANZAS
        insert;
        Fieldbyname('Cargo_cc_id').Asinteger := qryCargosPendientesDOCTO_CC_ID.Value;
        FieldByName('folio').AsString := qryCargosPendientesFOLIO.Value;
        FieldByName('fecha').Value := qryCargosPendientesFECHA.Value;
        FieldByName('cobrador_id').Value := qryCobradores.FieldByName('cobrador_id').Value;
        FieldByName('fechahora_salida').Value := Now;
        FieldByName('tipo_salida').Value := qryCargosPendientesTIPO_SALIDA.Value;
        Post;
      end;
    end;
    qryCargosPendientes.Next;
  end;


     //RECORRE TODOS LAS REMISIONEs PENDIENTES
     if chkRemisiones.Checked then
      begin
        qryRemisiones.First;
        while not qryRemisiones.Eof do
        begin
          //VALIDA SI ESTA SELECCIONADO
          if qryRemisionesENTREGADO.Value = 'S' then
          begin
            with qryCobranza do
            begin
              //REVISA SI NO ESTA REGISTRADA UNA SALIDA DE ESE DOCUMENTO EN LA TABLA DE COBRANZAS
              insert;
              Fieldbyname('Cargo_cc_id').Asinteger := qryRemisionesDOCTO_VE_ID.Value;
              FieldByName('folio').AsString := qryRemisionesFOLIO.Value;
              FieldByName('fecha').Value := qryRemisionesFECHA.Value;
              FieldByName('cobrador_id').Value := qryCobradores.FieldByName('cobrador_id').Value;
              FieldByName('fechahora_salida').Value := Now;
              FieldByName('tipo_salida').Value := 'R';
              Post;
               Conexion.ExecSQL('insert into sic_cobranza_det(id,cargo_cc_id,fecha,folio) values (-1,:cargo_cc_id,current_date,:folio)',[qryRemisionesDOCTO_VE_ID.Value,folio_str]);
               Conexion.ExecSQL('update doctos_ve set cobrador_id=:cobradorid where docto_ve_id=:id',[qryCobradores.FieldByName('cobrador_id').Value,qryRemisionesDOCTO_VE_ID.Value]);
            end;
          end;
          qryRemisiones.Next;
        end;
      end;
  ShowMessage('Salida creada con el folio: '+folio_str);



end;


procedure TCobranza.GenerarSalidaRemisiones;
begin
  //RECORRE TODOS LAS REMISIONEs PENDIENTES
  qryRemisiones.First;
  while not qryRemisiones.Eof do
  begin
    //VALIDA SI ESTA SELECCIONADO
    if qryRemisionesENTREGADO.Value = 'S' then
    begin
      with qryCobranza do
      begin
        //REVISA SI NO ESTA REGISTRADA UNA SALIDA DE ESE DOCUMENTO EN LA TABLA DE COBRANZAS
        insert;
        Fieldbyname('Cargo_cc_id').Asinteger := qryRemisionesDOCTO_VE_ID.Value;
        FieldByName('folio').AsString := qryRemisionesFOLIO.Value;
        FieldByName('fecha').Value := qryRemisionesFECHA.Value;
        FieldByName('cobrador_id').Value := qryCobradores.FieldByName('cobrador_id').Value;
        FieldByName('fechahora_salida').Value := Now;
        FieldByName('tipo_salida').Value := 'R';
        Post;

         Conexion.ExecSQL('update doctos_ve set cobrador_id=:cobradorid where docto_ve_id=:id',[qryCobradores.FieldByName('cobrador_id').Value,qryRemisionesDOCTO_VE_ID.Value]);
      end;
    end;
    qryRemisiones.Next;
  end;
end;

procedure TCobranza.ReporteSalida;
var
Memo: TfrxMemoView;
serie_factura:string;
begin
   serie_factura:=Conexion.ExecSQLScalar('select serie from folios_ventas where tipo_docto=''F''');
  qrysalida.SQL.Text:='select (select nombre from cobradores where cobrador_id=sc.cobrador_id) as Cobrador, (case when sc.tipo_salida is null then ''Factura'''+
             ' when sc.tipo_salida = ''F'' then ''Factura'''+
             ' when sc.tipo_salida = ''C'' then ''Contra recibo'''+
       ' end) as tipo_salida,(select nombre from clientes where cliente_id=(select cliente_id from doctos_cc c'+
  ' where c.docto_cc_id=sc.cargo_cc_id )) as nombre_cliente,'+
       ' sc.folio,sc.fecha,sc.fechahora_salida,scd.fecha as fecha_g,'+
  ' (select (importe+impuesto) as importe from importes_doctos_cc c'+
  ' where c.docto_cc_id=sc.cargo_cc_id )'+
 ' from sic_cobranza sc'+
 ' left join sic_cobranza_det scd on scd.cargo_cc_id=sc.cargo_cc_id'+
 ' where scd.folio=:folio and sc.folio like '''+serie_factura+'%''';
  qrySalida.ParamByName('folio').Value:=folio_str;
  qrySalida.Open;
          if chkRemisiones.Checked then
        begin
        qryRemisiones1.SQL.Text:='select (select nombre from cobradores where cobrador_id=sc.cobrador_id) as Cobrador,(select importe_neto from doctos_ve where docto_ve_id=sc.cargo_cc_id) as Importe,'+
' (select nombre from clientes where cliente_id=(select cliente_id from doctos_ve where docto_ve_id=sc.cargo_cc_id)) as Nombre,'+
                                  ' sc.folio,sc.fecha,sc.fechahora_salida,'+
                                  ' (case when sc.tipo_salida is null then ''Factura'''+
                                  ' when sc.tipo_salida = ''F'' then ''Factura'''+
                                  ' when sc.tipo_salida = ''C'' then ''Contra recibo'''+
                                  ' end) as tipo_salida,c.importe_orig_cargo as saldo_cargo,scd.fecha as fecha_g'+
                                  ' from sic_cobranza_det scd join'+
                                  ' sic_cobranza sc on scd.cargo_cc_id=sc.cargo_cc_id left join'+
                                  ' get_cargos_cc(current_date,''T'',0,''S'',''S'',''N'') c on'+
                                  ' c.docto_cc_id=sc.cargo_cc_id'+
                                  ' where scd.folio=:folio and sc.folio like ''R%'''+
                                  ' order by sc.fechahora_salida';
        qryRemisiones1.ParamByName('folio').Value := folio_str;
        qryRemisiones1.Open();
        qryRemisiones1.Refresh;
        end;
  Memo := rptSalida.FindObject('Memo18') as TfrxMemoView;
  Memo.Text:= folio_str;
  rptSalida.Print;
  rptSalida.ShowReport(true);
end;

procedure TCobranza.btnBuscarClick(Sender: TObject);
begin
  cargadatos;
end;

procedure TCobranza.btnGenerarSalidaClick(Sender: TObject);
begin
  GenerarSalida;
  if chkRemisiones.Checked then GenerarSalidaRemisiones;
  ReporteSalida;
  PageControl1.ActivePage:=tsRecepcion;
  cargadatos;
  if PageControl1.ActivePage.Caption='Recepcion de Cobranza' then
  begin
  qryCobradoresRecepcion.Open();
  qrycargosCobrador.ParamByName('cobrador_id').Value := qryCobradoresRecepcion.FieldByName('cobrador_id').AsInteger;
  qrycargosCobrador.Open();
  qrycargosCobrador.Refresh;
  end;

  if PageControl1.ActivePage.Caption='Reportes' then
  begin
  qryReportesFecha.Open();
  end;
end;

procedure TCobranza.btnGuardarRecepcionClick(Sender: TObject);
var query_str,fecha,clave_cliente,serie,serie1,folio_str,folio_str1,folio_nuevo,folio_nuevo1,forma,serie_factura,nombre_cobro:string;
 docto_cc_acr_id,consecutivo,consecutivo1,documento_id,concepto_id,concepto_id1,Forma_cobro,sucursal_id:integer;
Memo:TfrxMemoView;
begin
 try
  nombre_cobro:=Conexion.ExecSQLScalar('select nombre from conceptos_cc where concepto_cc_id=:cid',[Conexion.ExecSQLScalar('select concepto_cc_id from SIC_COBRANZA_PREF')]);

   //GENERAR FOLIO NUEVO
  try
  serie1 := GetSerie(Conexion.ExecSQLScalar('select SIG_FOLIO from conceptos_cc where upper(nombre) = ''RECEPCION DE COBRANZA'''));
  consecutivo1 := strtoint(GetnumFolio(Conexion.ExecSQLScalar('select SIG_FOLIO from conceptos_cc where upper(nombre) = ''RECEPCION DE COBRANZA''')));
  concepto_id1 := Conexion.ExecSQLScalar('select concepto_cc_id from conceptos_cc where upper(nombre) = ''RECEPCION DE COBRANZA''');
  folio_str1 := serie1 + Format('%.*d',[9-length(serie1),(consecutivo1)]);
  folio_nuevo1 := serie1 + Format('%.*d',[9-length(serie1),(consecutivo1+1)]);
  Conexion.ExecSQL('update conceptos_cc set sig_folio=:sig where concepto_cc_id=:concepto_id',[folio_nuevo1,concepto_id1]);
  except
  ShowMessage('Falta Concepto Recepcion de Cobranza');
  end;


 //RECORRE TODOS LOS CARGOS DE COBRADOR
  qryCargosCobrador.First;

    // RECORRER REMISIONES RECEPCION
    if CheckBox5.Checked then
     begin
      qryRemisionesRecepcion.First;
      while not qryRemisionesRecepcion.Eof do
       begin
          //VALIDA SI ESTA SELECCIONADO
          if qryRemisionesRecepcionENTREGADO.Value = 'S' then
          begin
            with qryRemisionesRecepcion do
               begin

                 Conexion.ExecSQL('insert into sic_cobranza_det(id,cargo_cc_id,fecha,folio) values (-1,:cargo_cc_id,current_date,:folio)',[qryRemisionesRecepcionCARGO_CC_ID.Value,folio_str1]);
                 Conexion.ExecSQL('update sic_cobranza set entrega_definitiva=''S'' where folio=:folio',[qryRemisionesRecepcionFOLIO.Value]);


           end;
          end;
             qryRemisionesRecepcion.Next;
       end;
     end;




   // begin
      while not qryCargosCobrador.Eof do
       begin
          //VALIDA SI ESTA SELECCIONADO
          if qryCargosCobradorENTREGA_DEFINITIVA.Value = 'S' then
          begin
            with qryCargosCobrador do
               begin
                           //INSERTAR EN LA TABLA DE SIC_COBRANZA_DET
        Conexion.ExecSQL('insert into sic_cobranza_det(id,cargo_cc_id,fecha,folio) values (-1,:cargo_cc_id,current_date,:folio)',[qrycargosCobradorDOCTO_CC_ID.Value,folio_str1]);
               if (qrycargosCobradorTIPO_ENTREGA.Value='E') or (qrycargosCobradorTIPO_ENTREGA.Value='T') or (qrycargosCobradorTIPO_ENTREGA.Value='C') then
               begin
                Conexion.ExecSQL('update sic_cobranza set tipo_entrega=:tipo_entrega,aclaraciones=:aclaraciones,fechahora_entrega=:fechahora,entrega_definitiva=''S'' where folio=:folio',[qrycargosCobradorTIPO_ENTREGA.Value,qrycargosCobradorACLARACIONES.Value,Now,qrycargosCobradorFOLIO.Value]);
               end
                else
                if (qrycargosCobradorTIPO_ENTREGA.Value='V') then
                begin
                 Conexion.ExecSQL('update sic_cobranza set tipo_entrega=:tipo_entrega,aclaraciones=:aclaraciones,fechahora_entrega=:fechahora,entrega_definitiva=''S'',prox_visita=:visita where folio=:folio',[qrycargosCobradorTIPO_ENTREGA.Value,qrycargosCobradorACLARACIONES.Value,Now,qrycargosCobradorPROX_VISITA.Value,qrycargosCobradorFOLIO.Value]);
                end
                else
               begin
                 Conexion.ExecSQL('update sic_cobranza set tipo_entrega=:tipo_entrega,aclaraciones=:aclaraciones,fechahora_entrega=:fechahora,entrega_definitiva=''S'' where folio=:folio',[qrycargosCobradorTIPO_ENTREGA.Value,qrycargosCobradorACLARACIONES.Value,Now,qrycargosCobradorFOLIO.Value]);
                end;

           end;

          sucursal_id:=Conexion.ExecSQLScalar('select sucursal_id from sucursales where nombre=''Matriz''');
          fecha := FormatDateTime('dd.mm.yyyy', Now);
          serie := GetSerie(Conexion.ExecSQLScalar('select SIG_FOLIO from conceptos_cc where nombre = :c',[nombre_cobro]));
          consecutivo := strtoint(GetnumFolio(Conexion.ExecSQLScalar('select SIG_FOLIO from conceptos_cc where nombre = :c',[nombre_cobro])));
          concepto_id := Conexion.ExecSQLScalar('select concepto_cc_id from conceptos_cc where nombre = :c',[nombre_cobro]);
          folio_str := serie + Format('%.*d',[9-length(serie),(consecutivo)]);
          folio_nuevo := serie + Format('%.*d',[9-length(serie),(consecutivo+1)]);
          Conexion.ExecSQL('update conceptos_cc set sig_folio=:sig where concepto_cc_id=:concepto_id',[folio_nuevo,concepto_id]);
          clave_cliente := Conexion.ExecSQLScalar('select coalesce(clave_cliente,'''') from get_clave_cli(:aid)',[qrycargosCobradorCLIENTE_ID.Value]);
          //AGREGAR DATOS A DOCUMENTOS CUENTAS POR COBRAR
           //****************************************DA DE ALTA EL ENCABEZADO***********************************
          if (qrycargosCobradorTIPO_ENTREGA.Value='E') or (qrycargosCobradorTIPO_ENTREGA.Value='T') or (qrycargosCobradorTIPO_ENTREGA.Value='C') then
            begin
            //MSP 2019
            {query_str:='insert into doctos_cc(docto_cc_id,concepto_cc_id,folio,naturaleza_concepto,fecha,clave_cliente,importe_cobro,'+
                       'cliente_id,tipo_cambio,aplicado,sistema_origen) values (-1,''11'','''+folio_str+''',''R'','''+fecha+''','''+clave_cliente+''',''0'','+inttostr(qrycargosCobradorCLIENTE_ID.Value)+',''1'',''S'',''CC'') returning docto_cc_id'; }
            //MSP 2020
           query_str:='insert into doctos_cc(docto_cc_id,concepto_cc_id,folio,naturaleza_concepto,sucursal_id,fecha,clave_cliente,importe_cobro,'+
                       'cliente_id,tipo_cambio,aplicado,sistema_origen) values (-1,'''+inttostr(concepto_id)+''','''+folio_str+''',''R'','''+inttostr(sucursal_id)+''','''+fecha+''','''+clave_cliente+''',''0'','+inttostr(qrycargosCobradorCLIENTE_ID.Value)+',''1'',''S'',''CC'') returning docto_cc_id';
            documento_id := Conexion.ExecSQLScalar(query_str);
            forma:=qrycargosCobradorTIPO_ENTREGA.Value;
            if forma='C' then forma:='Cheque';
            if forma='E' then forma:='Efectivo';
            if forma='T' then forma:='Transferencia';
            forma_cobro:=Conexion.ExecSQLScalar('select forma_cobro_cc_id from formas_cobro_cc where upper(nombre)=upper(:nombre)',[forma]);
            //**********************INSERTAR FORMA DE COBRO***********************//
            if (forma='Cheque') or (forma='Efectivo') or (forma='Transferencia') then
              begin
              query_str :='insert into formas_cobro_doctos(FORMA_COBRO_DOC_ID,NOM_TABLA_DOCTOS,'+
              'DOCTO_ID,FORMA_COBRO_ID,NUM_CTA_PAGO,CLAVE_SIS_FORMA_COB,REFERENCIA,'+
              'IMPORTE) values (-1,''DOCTOS_CC'','+inttostr(documento_id)+','+inttostr(Forma_cobro)+',NULL,''CC'',NULL,'+CurrToStr(qrycargosCobradorSALDO_CARGO.Value)+')';
              Conexion.ExecSQL(query_str);
              Conexion.Commit;
              end;

           //*******************************DETALLADO DEL DOCUMENTO*********************************************
            Conexion.ExecSQL('update sic_cobranza set folio_pagado=:foliopagado where folio=:folio',[folio_str,qrycargosCobradorFOLIO.Value]);

             try
            docto_cc_acr_id:=qrycargosCobradorDOCTO_CC_ID.Value;
              query_str := 'insert into importes_doctos_cc (impte_docto_cc_id, docto_cc_id,'+
              ' fecha,aplicado,tipo_impte,docto_cc_acr_id,importe,impuesto,iva_retenido,isr_retenido,dscto_ppag,pctje_comis_cob) values(-1,'+
              ' '+inttostr(documento_id)+','''+fecha+''',''S'',''R'','+inttostr(docto_cc_acr_id)+','''+CurrToStr(qrycargosCobradorSALDO_CARGO.Value)+''','+
              ' ''0'',''0'',''0'',''0'',''0'')';
              Conexion.ExecSQL(query_str);
              Conexion.Commit;
            except
             ShowMessage('El saldo del cargo es mayor al del credito.');
            end;
           end;


          end;
           qryCargosCobrador.Next;
       end;

        ShowMessage('Recepcion creada con el folio: '+folio_str1);
             //CREAR REPORTE////////////////////////
             qrySalidaRecepcion.SQL.Clear;
            { qrySalidaRecepcion.SQL.Text:='select (select nombre from cobradores where cobrador_id=sc.cobrador_id) as Cobrador,'+
                                    ' c.nombre_cliente,sc.folio,sc.fecha,sc.fechahora_salida,sc.fechahora_entrega,'+
                                    ' sc.folio_pagado,(case'+
                                    ' when sc.tipo_entrega= ''C'' then ''Credito'''+
                                    ' when sc.tipo_entrega = ''E'' then ''Efectivo'''+
                                    ' when sc.tipo_entrega= ''T'' then ''Transferencia'''+
                                    ' when sc.tipo_entrega= ''R'' then ''Contra Recibo'''+
                                    ' when sc.tipo_entrega= ''V'' then ''Prox Visita'''+
                                    ' end) as tipo_entrega,sc.aclaraciones,sc.prox_visita,'+
                                    ' (case when sc.tipo_salida is null then ''Factura'''+
                                    ' when sc.tipo_salida = ''F'' then ''Factura'''+
                                    ' when sc.tipo_salida = ''C'' then ''Contra recibo'''+
                                    ' end) as tipo_salida,c.importe_orig_cargo as saldo_cargo,scd.fecha as fecha_g'+
                                    ' from sic_cobranza_det scd left join'+
                                    ' sic_cobranza sc on scd.cargo_cc_id=sc.cargo_cc_id left join'+
                                    ' get_cargos_cc(current_date,''T'',0,''S'',''S'',''N'') c on'+
                                    ' c.docto_cc_id=sc.cargo_cc_id'+
                                    ' where scd.folio=:folio'+
                                    ' order by sc.fechahora_salida';}
                                    serie_factura:=Conexion.ExecSQLScalar('select serie from folios_ventas where tipo_docto=''F''');
              qrySalidaRecepcion.SQL.Text:='select sc.fechahora_entrega,sc.folio_pagado,'+
'(case when sc.tipo_entrega= ''C'' then ''Credito'' when'+
' sc.tipo_entrega = ''E'' then ''Efectivo'' when sc.tipo_entrega= ''T'' then'+
' ''Transferencia'' when sc.tipo_entrega= ''R'' then ''Contra Recibo'' when'+
' sc.tipo_entrega= ''V'' then ''Prox Visita'' end) as tipo_entrega,sc.aclaraciones,'+
' sc.prox_visita, (select nombre from cobradores where cobrador_id=sc.cobrador_id)'+
' as cobrador, (select nombre from clientes where cliente_id=(select cliente_id from'+
 ' doctos_cc where docto_cc_id=sc.cargo_cc_id)) as Nombre, sc.folio,sc.fecha,'+
 ' sc.fechahora_salida, (case when sc.tipo_salida is null then ''Factura'' when'+
  ' sc.tipo_salida = ''F'' then''Factura'' when sc.tipo_salida = ''C'' then'+
  ' ''Contra recibo'' end) as tipo_salida,'+
  ' scd.fecha as fecha_g from sic_cobranza_det scd join sic_cobranza sc on'+
  ' scd.cargo_cc_id=sc.cargo_cc_id where scd.folio=:folio and sc.folio'+
   ' like '''+serie_factura+'%'' order by sc.fechahora_salida';
        qrySalidaRecepcion.ParamByName('folio').Value := folio_str1;
        qrySalidaRecepcion.Open;
        Memo := rptRecepcion.FindObject('Memo18') as TfrxMemoView;
        Memo.Text:= folio_str1;
        if CheckBox5.Checked then
        begin
        qryRemisionesRecepcion1.ParamByName('cobrador_id').Value := qryCobradoresRecepcion.FieldByName('cobrador_id').AsInteger;
        qryRemisionesRecepcion1.Open;
        qryRemisionesRecepcion1.Refresh;
        end;
        rptRecepcion.PrepareReport(True);
        //      rptRecepcion.Print;
        rptRecepcion.ShowReport(True);
              /////////////////////////////////////
   grdcargosCobrador.DataSource.DataSet.Refresh;
   qrycargosCobrador.First;
   grdCargosCobrador.Refresh;
 // qryCargosPendientes.Refresh;
   grdCargosSalida.Refresh;
   grdRemisionesRecepcion.Refresh;
   grdRemisiones.Refresh;
   PageControl1.ActivePageIndex:=0;
              except
           ShowMessage('No existe concepto de cobro en pestaña Admin.');
           end;
end;

procedure TCobranza.btnNuevaContraseñaClick(Sender: TObject);
var
  FormAdmin:TCobranzaAdmin;
begin
  FormAdmin := TCobranzaAdmin.Create(nil);
  FormAdmin.Conexion.Params := Conexion.Params;
  FormAdmin.StatusBar1.Panels.Items[0].Text := StatusBar1.panels.items[0].text;
  FormAdmin.ShowModal;
end;

procedure TCobranza.btnPassClick(Sender: TObject);
var
  FormAdmin:TCobranzaAdminCambiar;
begin
  FormAdmin := TCobranzaAdminCambiar.Create(nil);
  FormAdmin.Conexion.Params := Conexion.Params;
  FormAdmin.StatusBar1.Panels.Items[0].Text := StatusBar1.panels.items[0].text;
  FormAdmin.ShowModal;
end;

procedure TCobranza.btnTransitoClick(Sender: TObject);
begin
qryTransito.sql.Text:=' select co.folio,co.fecha,(select nombre from clientes where cliente_id=(select cliente_id from doctos_cc c'+
  ' where c.docto_cc_id=co.cargo_cc_id )) as nombre_cliente,'+
   ' (select cliente_id from doctos_cc c'+
   ' where c.docto_cc_id=co.cargo_cc_id ),'+
  ' co.cargo_cc_id as docto_cc_id,'+
       ' (case when co.tipo_salida is null then ''Factura'''+
             ' when co.tipo_salida = ''F'' then ''Factura'''+
             ' when co.tipo_salida = ''C'' then ''Contra recibo'''+
       ' end) as tipo_SALIDA,co.fechahora_salida,co.cobrador_id,'+
       ' cr.nombre as cobrador,co.fechahora_entrega,co.tipo_entrega as tipo_entrega,'+
       ' co.aclaraciones as aclaraciones,current_date as prox_visita,'+
        '''N'' as entrega_definitiva,co.folio_pagado,'+
       ' (select (importe+impuesto) as saldo_cargo from importes_doctos_cc c'+
  ' where c.docto_cc_id=co.cargo_cc_id )'+
' from sic_cobranza co left join'+
' cobradores cr on cr.cobrador_id = co.cobrador_id'+
' where co.cobrador_id = :cobrador_id'+
' order by co.fechahora_salida';
qryTransito.ParamByName('cobrador_id').Value:=qryReportesFecha.FieldByName('cobrador_id').AsInteger;
qryTransito.Open();
qryTransito.Refresh;
rptTransito.PrepareReport(True);
rptTransito.Print;
rptTransito.ShowReport(True);
end;

procedure TCobranza.Button1Click(Sender: TObject);
begin
   consultareporte;
                                            //CREAR REPORTE////////////////////////

             qryFecha.Open;
             qryFecha.Refresh;
               qryRemisionesRecepcion.Open;
          qryRemisionesRecepcion.Refresh;
             rptFecha.PrepareReport(True);
             rptFecha.Print;
    //  rptFecha.ShowReport(True);
              /////////////////////////////////////
end;
  procedure TCobranza.Button2Click(Sender: TObject);
begin
     consultafactura;
     if qryFolio.RecordCount=0 then
        begin
        ShowMessage('Folio no encontrado.');
        end
        else
        begin
                   //CREAR REPORTE///////////////////////
        qryFolio.Refresh;
        rptFolio.PrepareReport(True);
        rptFolio.Print;
        rptFolio.ShowReport(True);
        end;
end;

procedure TCobranza.consultareporte;
  var
  Memo,Memo2,Memo3,Memo4: TfrxMemoView;
liquidado,fecha,prox_visita:string;
  begin
     if RadioGroup1.ItemIndex=0 then
         begin
         prox_visita:='';
         end;
          if RadioGroup1.ItemIndex=1 then
         begin
         prox_visita:=' and tipo_entrega=''V''';
         end;


    if CheckBox2.Checked then
    begin
       fecha:=' where co.cobrador_id = :cobrador_id and c.fecha between :FECHA_INI and :FECHA_FIN';
    end
    else
    begin
         fecha:=' where co.cobrador_id = :cobrador_id';
    end;
        qryFecha.SQL.Text := ' select  co.fecha,co.folio,(select nombre from clientes where cliente_id=(select cliente_id from doctos_cc c'+
  ' where c.docto_cc_id=co.cargo_cc_id )) as nombre_cliente,(select cliente_id from doctos_cc c'+
  ' where c.docto_cc_id=co.cargo_cc_id ),(select (importe+impuesto) as saldo_cargo from importes_doctos_cc c'+
  ' where c.docto_cc_id=co.cargo_cc_id ),'+
' (case when co.tipo_salida is null then ''Factura''when'+
' co.tipo_salida = ''F'' then ''Factura'' when co.tipo_salida = ''C'' then'+
  '''Contra recibo'' end) as tipo_SALIDA,co.fechahora_salida,c.docto_cc_id,'+
 ' co.cobrador_id,cr.nombre as cobrador,co.fechahora_entrega,'+
 ' (case when co.tipo_entrega= ''C'' then ''Credito''when co.tipo_entrega = ''E'''+
 ' then ''Efectivo'' when co.tipo_entrega= ''T'' then ''Transferencia'' when'+
 ' co.tipo_entrega= ''R'' then ''Contra Recibo'' when co.tipo_entrega= ''V'''+
 ' then ''Prox Visita'' end) as tipo_entrega,co.aclaraciones as'+
 ' aclaraciones,prox_visita,''N'' as entrega_definitiva,co.folio_pagado'+
 ' from doctos_cc c left join'+
  ' sic_cobranza co on co.cargo_cc_id = c.docto_cc_id left join cobradores'+
  ' cr on cr.cobrador_id = co.cobrador_id'+
          fecha+prox_visita+
    ' order by co.fechahora_salida';

  qryFecha.ParamByName('cobrador_id').Value := qryReportesFecha.FieldByName('cobrador_id').AsInteger;

  //RECARGAR DATOS QUERY REMISIONESRECEPCION
    {qryRemisionesRecepcion1.SQL.Text := 'select ''N'' as entregado,sc.folio,fechahora_salida as fecha,'+
                                        'cli.nombre as cliente,c.nombre as cobrador,sc.folio_pagado,dve.cliente_id,sc.entrega_definitiva,dve.importe_neto as importe'+
                                        ' from sic_cobranza sc join'+
                                        ' cobradores c on sc.cobrador_id=c.cobrador_id join'+
                                        ' doctos_ve dve on dve.docto_ve_id=sc.cargo_cc_id'+
                                        ' join clientes cli on cli.cliente_id=dve.cliente_id'+
                                        ' where sc.tipo_salida=''R'' and sc.cobrador_id=:cobrador_id  and sc.entrega_definitiva=''S'''+
                                        ' order by fechahora_salida';}
 { qryRemisionesRecepcion1.SQL.Text:='select (select nombre from cobradores where cobrador_id=sc.cobrador_id) as Cobrador,(select importe_neto from doctos_ve where docto_ve_id=sc.cargo_cc_id) as Importe,'+
'(select nombre from clientes where cliente_id=(select cliente_id from doctos_ve where docto_ve_id=sc.cargo_cc_id)) as Nombre,'+
'sc.folio,sc.fecha,sc.fechahora_salida,'+
'(case when sc.tipo_salida is null then ''Factura'''+
             ' when sc.tipo_salida = ''F'' then ''Factura'''+
             ' when sc.tipo_salida = ''C'' then ''Contra recibo'''+
       ' end) as tipo_salida,c.importe_orig_cargo as saldo_cargo,scd.fecha as fecha_g'+
' from sic_cobranza_det scd join'+
' sic_cobranza sc on scd.cargo_cc_id=sc.cargo_cc_id left join'+
' get_cargos_cc(current_date,''T'',0,''S'',''S'',''N'') c on'+
' c.docto_cc_id=sc.cargo_cc_id'+
' where c.cobrador_id=:cobrador_id and sc.tipo_salida=''R'''+
' order by sc.fechahora_salida';
  qryRemisionesRecepcion1.ParamByName('cobrador_id').Value := qryReportesFecha.FieldByName('cobrador_id').AsInteger;
  qryRemisionesRecepcion1.Open; }

            if CheckBox2.Checked then
            begin
            qryFecha.ParamByName('FECHA_INI').AsDateTime := DateTimePicker2.DateTime;
            qryFecha.ParamByName('FECHA_FIN').AsDateTime := DateTimePicker3.DateTime;
                            Memo := rptFecha.FindObject('Memo4') as TfrxMemoView;
                Memo.Text:= datetostr(DateTimePicker2.DateTime);
                  Memo2 := rptFecha.FindObject('Memo8') as TfrxMemoView;
                  Memo2.Text:= datetostr(DateTimePicker3.DateTime);
                          Memo3 := rptFecha.FindObject('Memo10') as TfrxMemoView;
                  Memo3.Text:='Fecha del';
                          Memo4 := rptFecha.FindObject('Memo12') as TfrxMemoView;
                  Memo4.Text:='al';
            end
            else
            begin
                                          Memo := rptFecha.FindObject('Memo4') as TfrxMemoView;
                Memo.Text:= '';
                  Memo2 := rptFecha.FindObject('Memo8') as TfrxMemoView;
                  Memo2.Text:='';
                          Memo3 := rptFecha.FindObject('Memo10') as TfrxMemoView;
                  Memo3.Text:='';
                          Memo4 := rptFecha.FindObject('Memo12') as TfrxMemoView;
                  Memo4.Text:='';
            end;
  end;

  procedure Tcobranza.consultafactura;
  var tipo_salida:integer; Memo,Memo1,Memo2,Memo3,Memo4,Memo5: TfrxMemoView;
  begin
   if Length(txtFolio.Text)<9 then
      begin
       ShowMessage('Folio debe de ser de 9 caracteres.');
      end
      else
      begin
      qryFolio.SQL.Clear;
      qryFolio.SQL.Text:='select * from sic_cobranza co left join'+
        ' cobradores cr on cr.cobrador_id = co.cobrador_id'+
        ' join doctos_ve dve on dve.folio=co.folio'+
        ' where co.folio = :folio'+
        ' order by co.fechahora_salida';
      qryFolio.ParamByName('folio').Value := txtFolio.Text;
      qryFolio.Open;

        tipo_salida := Conexion.ExecSQLScalar('select * from sic_cobranza co left join'+
        ' cobradores cr on cr.cobrador_id = co.cobrador_id'+
        ' where co.folio = '''+txtFolio.Text+''' and co.tipo_salida=''R'''+
        ' order by co.fechahora_salida');
      if tipo_salida>=1 then
          begin
             Memo := rptFolio.FindObject('Memo4') as TfrxMemoView;
                Memo.Text:= 'Remision';
                 Memo1 := rptFolio.FindObject('Memo6') as TfrxMemoView;
                Memo1.Text:= 'Importe';
                 Memo2 := rptFolio.FindObject('dsFolioFOLIO_PAGADO') as TfrxMemoView;
                Memo2.DataField:= 'IMPORTE_NETO';
                             Memo3 := rptFolio.FindObject('Memo2') as TfrxMemoView;
                Memo3.Text:= '';
                             Memo4 := rptFolio.FindObject('Memo11') as TfrxMemoView;
                Memo4.Text:= '';
                             Memo5 := rptFolio.FindObject('Memo13') as TfrxMemoView;
                Memo5.Text:= '';

          end;

      end;
  end;

  procedure Tcobranza.consultasalida;
  var tipo_salida:integer; Memo: TfrxMemoView; folio_salida,folio,serie_factura:string;
  begin
      serie_factura:=Conexion.ExecSQLScalar('select serie from folios_ventas where tipo_docto=''F''');
      qrySalida.SQL.Clear;
        qrySalida.SQL.Text:='select (select nombre from cobradores where cobrador_id=sc.cobrador_id) as Cobrador,'+
  ' (case when sc.tipo_salida is null then ''Factura'''+
             ' when sc.tipo_salida = ''F'' then ''Factura'''+
             ' when sc.tipo_salida = ''C'' then ''Contra recibo'''+
       ' end) as tipo_salida,(select nombre from clientes where cliente_id=(select cliente_id from doctos_cc c'+
   ' where c.docto_cc_id=sc.cargo_cc_id )) as nombre_cliente,'+
       ' sc.folio,sc.fecha,sc.fechahora_salida,scd.fecha as fecha_g,'+
  ' (select (importe+impuesto) as importe from importes_doctos_cc c'+
  ' where c.docto_cc_id=sc.cargo_cc_id )'+
  ' from sic_cobranza sc'+
  ' left join sic_cobranza_det scd on scd.cargo_cc_id=sc.cargo_cc_id'+
  ' where scd.folio=:folio and sc.folio like '''+serie_factura+'%''';
            if Length(txtFolioSalida.Text)<5 then
            begin
            folio:=txtFolioSalida.Text;
            if folio[1]='R' then
             begin
             qrySalida.ParamByName('folio').Value:=folio[1]+Format('%.*d',[9-folio.Length+(folio.Length-1), strtoint(folio.Substring(1))]);
              end
              else
              begin
              qrySalida.ParamByName('folio').Value:=folio[1]+Format('%.*d',[9-folio.Length+(folio.Length-1)  , strtoint(folio.Substring(1))]);
              end;
              end
            else
            begin
            qrySalida.ParamByName('folio').Value:=txtFolioSalida.Text;
            end;
        qrySalida.Open;

      folio_salida:=txtFolioSalida.Text;
      if folio_salida[1]='R' then
        begin
        Memo := rptFolioSalida.FindObject('Memo5') as TfrxMemoView;
        Memo.Text:= 'Recepcion de Cobranza';
        end;
      end;

procedure TCobranza.Button6Click(Sender: TObject);
var
Memo: TfrxMemoView; folio:string;
begin
  consultasalida;
     if qrySalida.RecordCount=0 then
        begin
        ShowMessage('Folio no encontrado.');
        end
        else
        begin
                   //CREAR REPORTE///////////////////////
                                      if Length(txtFolioSalida.Text)<5 then
            begin
            folio:=txtFolioSalida.Text;
            if folio[1]='R' then
             begin
             qryRemisiones1.ParamByName('folio').Value:=folio[1]+Format('%.*d',[9-folio.Length+(folio.Length-1), strtoint(folio.Substring(1))]);
              end
              else
              begin
              qryRemisiones1.ParamByName('folio').Value:=folio[1]+Format('%.*d',[9-folio.Length+(folio.Length-1)  , strtoint(folio.Substring(1))]);
              end;
              end
            else
            begin
            qryRemisiones1.ParamByName('folio').Value := txtFolioSalida.Text;
            end;
        qryRemisiones1.Open();
        qryRemisiones1.Refresh;
        Memo := rptFolioSalida.FindObject('Memo18') as TfrxMemoView;
        Memo.Text:= qryRemisiones1.ParamByName('folio').Value;
        rptFolioSalida.PrepareReport(True);
        rptFolioSalida.Print;
        rptFolioSalida.ShowReport(True);
        end;
end;



procedure TCobranza.Button7Click(Sender: TObject);
var
Memo: TfrxMemoView; folio:string;
begin
  consultasalida;
     if qrySalida.RecordCount=0 then
        begin
        ShowMessage('Folio no encontrado.');
        end
        else
        begin
                   //CREAR REPORTE///////////////////////
                               if Length(txtFolioSalida.Text)<5 then
            begin
            folio:=txtFolioSalida.Text;
            if folio[1]='R' then
             begin
             qryRemisiones1.ParamByName('folio').Value:=folio[1]+Format('%.*d',[9-folio.Length+(folio.Length-1), strtoint(folio.Substring(1))]);
              end
              else
              begin
              qryRemisiones1.ParamByName('folio').Value:=folio[1]+Format('%.*d',[9-folio.Length+(folio.Length-1)  , strtoint(folio.Substring(1))]);
              end;
              end
            else
            begin
            qryRemisiones1.ParamByName('folio').Value := txtFolioSalida.Text;
            end;

        qryRemisiones1.Open();
        qryRemisiones1.Refresh;
        Memo := rptFolioSalida.FindObject('Memo18') as TfrxMemoView;
        Memo.Text:= qryRemisiones1.ParamByName('folio').Value;
        rptFolioSalida.PrepareReport(True);
        rptFolioSalida.ShowReport(True);
        end;
end;

procedure TCobranza.Button3Click(Sender: TObject);
begin
   consultareporte;
          //CREAR REPORTE////////////////////////

          qryFecha.Open;
          qryRemisionesRecepcion.Open;
          qryRemisionesRecepcion.Refresh;
          rptFecha.PrepareReport(True);
          rptFecha.ShowReport(True);
              /////////////////////////////////////
end;

procedure TCobranza.Button4Click(Sender: TObject);
begin
  consultafactura;
     if qryFolio.RecordCount=0 then
        begin
        ShowMessage('Folio no encontrado.');
        end
        else
        begin
                   //CREAR REPORTE///////////////////////
        qryFolio.Refresh;
        rptFolio.PrepareReport(True);
        rptFolio.ShowReport(True);
        end;
end;

procedure TCobranza.Button5Click(Sender: TObject);
var
FormVerificar:TCobranzaVerificar;
begin
  if (Conexion.ExecSQLScalar('select pass from sic_cobranza_login where usuario=''Admin''')='') then
  begin
    ShowMessage('No hay contraseña guardada, entrar en la pestaña Admin para asignar una nueva.');
  end
  else
     begin
      FormVerificar:=TCobranzaVerificar.Create(nil);
      FormVerificar.Conexion.Params := Conexion.Params;
      FormVerificar.StatusBar1.Panels.Items[0].Text := StatusBar1.panels.items[0].text;
      FormVerificar.ShowModal;
     end;
     btnBuscar.Click;
end;

procedure TCobranza.cargadatos;
var fecha:string;
begin
  case rgOrden.ItemIndex of
     0: begin
         cbCliente.Visible:=false;
         cbVendedor.Visible:=false;
         cbZona.Visible:=false;
         fecha:=' where c.fecha between :FECHA_INI and :FECHA_FIN';
         orden := ' order by fecha' ;
         end;
     1: begin
         cbCliente.Visible:=false;
         cbVendedor.Visible:=false;
         cbZona.Visible:=false;
         fecha:=' where c.fecha between :FECHA_INI and :FECHA_FIN';
         orden := ' order by folio';
         end;
     2: begin
         cbCliente.Visible:=false;
         cbVendedor.Visible:=false;
         cbZona.Visible:=false;
         fecha:=' where c.fecha between :FECHA_INI and :FECHA_FIN';
         orden := ' order by nombre_cliente';
         end;
     3: begin
         cbCliente.Visible:=false;
         cbZona.Visible:=false;
         cbVendedor.Visible:=false;
         fecha:=' where c.fecha between :FECHA_INI and :FECHA_FIN';
         orden := ' order by dias_atraso desc';
         end;
     4: begin
        cbCliente.Visible:=true;
        cbVendedor.Visible:=false;
        cbZona.Visible:=false;
        fecha:='';
        qryClientes.Open();
        orden := ' where c.cliente_id='+inttostr(qryClientesCLIENTE_ID.Value)+' order by c.folio';
        end;
     5: begin
        cbCliente.Visible:=false;
        cbVendedor.Visible:=true;
        cbZona.Visible:=false;
        fecha:='';
        qryVendedores.Open();
                //cbVendedor.ItemIndex:=cbVendedor.Items.IndexOf(cbcobradores.Text);
              {  cbVendedor.SetFocus;
                keybd_event(VK_RETURN,0,0,0);}
        orden := ' where vendedor_id='+inttostr(qryVendedoresVENDEDOR_ID.Value)+' order by c.folio';
        end;
        6: begin
        cbCliente.Visible:=false;
        cbVendedor.Visible:=false;
        cbZona.Visible:=true;
        fecha:='';
        qryZona.Open();
        orden := ' where cl.zona_cliente_id='+inttostr(qryZonaZONA_CLIENTE_ID.Value)+' order by c.folio';
        end;
  end;
         //RECARGAR DATOS QUERY CARGOSPENDIENTES

         if PageControl1.ActivePage=tsSalida then
         begin
          try
          qryCargosPendientes.SQL.Text := 'select  (case when cr.nombre is null then ''S'' else ''N'' end) as entregado,'+
          ' c.fecha,c.folio,c.nombre_cliente,c.importe_orig_cargo,c.saldo_cargo,'+
          ' datediff(day from c.fecha_vencimiento to current_date) as dias_atraso,'+
          ' (case when co.tipo_salida is null then ''F'' else co.tipo_salida end) as tipo_salida,c.docto_cc_id,'+
          ' cr.nombre as cobrador,c.cliente_id,cl.vendedor_id'+
          ' from get_cargos_cc(current_date,''P'',0,''S'',''S'',''N'') c left join'+
           ' sic_cobranza co on co.cargo_cc_id = c.docto_cc_id left join'+
          ' cobradores cr on cr.cobrador_id = co.cobrador_id left join'+
          ' clientes cl on c.cliente_id=cl.cliente_id'+
              fecha + orden;
              if (rgOrden.ItemIndex=0) or (rgOrden.ItemIndex=1) or (rgOrden.ItemIndex=2) or (rgOrden.ItemIndex=3) then
              begin
              qryCargosPendientes.ParamByName('FECHA_INI').AsDateTime := dtpInicio.DateTime;
              qryCargosPendientes.ParamByName('FECHA_FIN').AsDateTime := dtpFin.DateTime;
              end;
          qryCargosPendientes.Open;
          except

          end;
         end;



   //RECARGAR DATOS QUERY REMISIONESRECEPCION
   { qryRemisiones.SQL.Text := 'select ''S'' as Entregado,dve.fecha,dve.folio,c.nombre as nombre_cliente,'+
                              'datediff(day from dve.fecha_vigencia_entrega to current_date) as dias_atraso,'+
                              'importe_neto as Importe,cr.nombre as cobrador,dve.docto_ve_id'+
                               ' from doctos_ve dve left join'+
                               ' clientes c on dve.cliente_id=c.cliente_id left join'+
                               ' cobradores cr on cr.cobrador_id = dve.cobrador_id'+
                               ' where dve.tipo_docto=''R'' and dve.estatus=''P'' and dve.fecha between :FECHA_INI and :FECHA_FIN'+ orden;
  qryRemisiones.ParamByName('FECHA_INI').AsDateTime := dtpInicio.DateTime;
  qryRemisiones.ParamByName('FECHA_FIN').AsDateTime := dtpFin.DateTime;
  qryRemisiones.Open; }


   //RECARGAR DATOS QUERY REMISIONESRECEPCION
     if chkRemisiones.Checked then
  begin
    qryRemisionesRecepcion.SQL.Text := 'select ''N'' as entregado,sc.cargo_cc_id,sc.folio,fechahora_salida as fecha,'+
                                        'cli.nombre as cliente,c.nombre as cobrador,sc.folio_pagado,dve.cliente_id,sc.entrega_definitiva,dve.importe_neto as importe'+
                                        ' from sic_cobranza sc join'+
                                        ' cobradores c on sc.cobrador_id=c.cobrador_id join'+
                                        ' doctos_ve dve on dve.docto_ve_id=sc.cargo_cc_id'+
                                        ' join clientes cli on cli.cliente_id=dve.cliente_id'+
                                        ' where sc.tipo_salida=''R'' and sc.cobrador_id=:cobrador_id'+
                                        ' order by fechahora_salida';
  qryRemisionesRecepcion.ParamByName('cobrador_id').Value := qryCobradoresRecepcion.FieldByName('cobrador_id').AsInteger;
  qryRemisionesRecepcion.Open;



          qryRemisiones.SQL.Text := 'select ''N'' as Entregado,dve.fecha,dve.folio,c.nombre as nombre_cliente,'+
                              'datediff(day from dve.fecha_vigencia_entrega to current_date) as dias_atraso,'+
                              'importe_neto as Importe,cr.nombre as cobrador,dve.docto_ve_id'+
                               ' from doctos_ve dve left join'+
                               ' clientes c on dve.cliente_id=c.cliente_id left join'+
                               ' cobradores cr on cr.cobrador_id = dve.cobrador_id'+
                               ' where dve.tipo_docto=''R'' and dve.estatus=''P'' and dve.fecha between :FECHA_INI and :FECHA_FIN'+ orden;

        qryRemisiones.ParamByName('FECHA_INI').AsDateTime := dtpInicio.DateTime;
        qryRemisiones.ParamByName('FECHA_FIN').AsDateTime := dtpFin.DateTime;
        qryRemisiones.Open;
  end;


  if txtbusquedaSalida.Text = '' then
  begin
    qryCargosPendientes.Filtered := False;
  end
  else
  begin
    qryCargosPendientes.Filter := 'upper(nombre_CLIENTE) like ''%'+txtbusquedaSalida.Text+'%'' or upper(folio) like ''%'+txtbusquedaSalida.Text+'%'' or upper(fecha) like ''%'+txtbusquedaSalida.Text+'%''';
    qryCargosPendientes.Filtered := True;
  end;
  lblNumCargos.Caption := inttostr(qryCargosPendientes.RecordCount)+ ' Cargos';
    lblNumRemisiones.Caption := inttostr(qryRemisiones.RecordCount)+ ' Remisiones';

end;

procedure TCobranza.cbAllClick(Sender: TObject);
begin
  qryCargosPendientes.DisableControls;
  qryCargosPendientes.First;
  while not qryCargosPendientes.Eof do
  begin
     if qryCargosPendientesCOBRADOR.Value <> '' then
        begin
        qryCargosPendientes.Edit;
        qryCargosPendientesENTREGADO.Value := 'N';
        qryCargosPendientes.Post;
        qryCargosPendientes.Next;
        end
        else
        begin
          if cbAll.Checked then
          begin
          qryCargosPendientes.Edit;
          qryCargosPendientesENTREGADO.Value := 'S';
          qryCargosPendientes.Post;
          qryCargosPendientes.Next;
          end
           else
           begin
            qryCargosPendientes.Edit;
            qryCargosPendientesENTREGADO.Value := 'N';
            qryCargosPendientes.Post;
            qryCargosPendientes.Next;
           end;
           end;
  end;
  qryCargosPendientes.First;
  qryCargosPendientes.EnableControls;
  CheckBox1.Checked := qryCargosPendientesENTREGADO.Value = 'S';
end;

procedure TCobranza.cbAllRemisionesClick(Sender: TObject);
begin
 qryRemisiones.DisableControls;
  qryRemisiones.First;
  while not qryRemisiones.Eof do
  begin
     if qryRemisionesCOBRADOR.Value <> '' then
        begin
        qryRemisiones.Edit;
        qryRemisionesENTREGADO.Value := 'N';
        qryRemisiones.Post;
        qryRemisiones.Next;
        end
        else
        begin
          if cbAllRemisiones.Checked then
          begin
          qryRemisiones.Edit;
          qryRemisionesENTREGADO.Value := 'S';
          qryRemisiones.Post;
          qryRemisiones.Next;
          end
           else
           begin
            qryRemisiones.Edit;
            qryRemisionesENTREGADO.Value := 'N';
            qryRemisiones.Post;
            qryRemisiones.Next;
           end;
           end;
  end;
  qryRemisiones.First;
  qryRemisiones.EnableControls;
  CheckBox3.Checked := qryRemisionesENTREGADO.Value = 'S';
end;

procedure TCobranza.cbCargosCobradorClick(Sender: TObject);
begin
  qrycargosCobrador.Edit;
  if cbCargosCobrador.Checked then
   if qrycargosCobradorFOLIO_PAGADO.Value <> '' then
     qrycargosCobradorENTREGA_DEFINITIVA.Value := 'N'
     else
    qrycargosCobradorENTREGA_DEFINITIVA.Value := 'S'
  else
    qrycargosCobradorENTREGA_DEFINITIVA.Value := 'N';
    qrycargosCobrador.Post;

end;

procedure TCobranza.cbCobradoresRecepcionChange(Sender: TObject);
begin
  qrycargosCobrador.ParamByName('cobrador_id').Value := qryCobradoresRecepcion.FieldByName('cobrador_id').AsInteger;
  qrycargosCobrador.Refresh;
  cargadatos;
end;

procedure TCobranza.cbVendedorChange(Sender: TObject);
begin
  cargadatos;
end;

procedure TCobranza.cbVendedorKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
 if (key=VK_RETURN) then
 begin
 cargadatos;
 end;
end;

procedure TCobranza.cbClienteChange(Sender: TObject);
begin
  cargadatos;
end;

procedure TCobranza.CheckBox1Click(Sender: TObject);
begin
  qryCargosPendientes.Edit;
  if CheckBox1.Checked then
  begin
  qryCargosPendientesENTREGADO.Value := 'S';
  if qryCargosPendientesCOBRADOR.Value <> '' then
    qryCargosPendientesENTREGADO.Value := 'N'
  end
    else
  qryCargosPendientesENTREGADO.Value := 'N';
  qryCargosPendientes.Post;


end;

procedure TCobranza.CheckBox2Click(Sender: TObject);
begin
  if CheckBox2.Checked then
    begin
      DateTimePicker2.Enabled:=True;
      DateTimePicker3.Enabled:=True;
    end
    else
    begin
         DateTimePicker2.Enabled:=False;
         DateTimePicker3.Enabled:=False;
    end;
end;

procedure TCobranza.CheckBox3Click(Sender: TObject);
begin
  qryRemisiones.Edit;
  if CheckBox3.Checked then
  begin
  qryRemisionesENTREGADO.Value := 'S';
  if qryRemisionesCOBRADOR.Value <> '' then
    qryRemisionesENTREGADO.Value := 'N'
  end
    else
  qryRemisionesENTREGADO.Value := 'N';
  qryRemisiones.Post;
end;

procedure TCobranza.CheckBox4Click(Sender: TObject);
begin
qryRemisionesRecepcion.DisableControls;
  qryRemisionesRecepcion.First;
  while not qryRemisionesRecepcion.Eof do
  begin
     if qryRemisionesRecepcionENTREGA_DEFINITIVA.Value = 'S' then
        begin
        qryRemisionesRecepcion.Edit;
        qryRemisionesRecepcionENTREGADO.Value := 'N';
        qryRemisionesRecepcion.Post;
        qryRemisionesRecepcion.Next;
        end
        else
        begin
          if CheckBox4.Checked then
          begin
          qryRemisionesRecepcion.Edit;
          qryRemisionesRecepcionENTREGADO.Value := 'S';
          qryRemisionesRecepcion.Post;
          qryRemisionesRecepcion.Next;
          end
           else
           begin
            qryRemisionesRecepcion.Edit;
            qryRemisionesRecepcionENTREGADO.Value := 'N';
            qryRemisionesRecepcion.Post;
            qryRemisionesRecepcion.Next;
           end;
           end;
  end;
  qryRemisionesRecepcion.First;
  qryRemisionesRecepcion.EnableControls;
  CheckBox6.Checked := qryRemisionesENTREGADO.Value = 'S';
end;

procedure TCobranza.CheckBox5Click(Sender: TObject);
begin
   if CheckBox5.Checked then
  begin
  self.Height:=self.Height+200;
  grdRemisionesRecepcion.Visible:=true;
  label10.Visible:=true;
  CheckBox4.Visible:=True;
      cargadatos;
  end
    else
    begin
    self.Height:=self.Height-200;
    grdRemisionesRecepcion.Visible:=false;
    label10.Visible:=false;
    CheckBox4.Visible:=false;
    CheckBox6.Visible:=false;
    end;

end;

procedure TCobranza.CheckBox6Click(Sender: TObject);
begin
 qryRemisionesRecepcion.Edit;
  if CheckBox6.Checked then
  begin
  qryRemisionesRecepcionENTREGADO.Value := 'S';
  if qryRemisionesRecepcionENTREGA_DEFINITIVA.Value = 'S' then
    qryRemisionesRecepcionENTREGADO.Value := 'N'
  end
    else
  qryRemisionesRecepcionENTREGADO.Value := 'N';
  qryRemisionesRecepcion.Post;
end;

procedure TCobranza.ChkAllRecepcionClick(Sender: TObject);
begin
 qrycargosCobrador.DisableControls;
  qrycargosCobrador.First;
  while not qrycargosCobrador.Eof do
  begin
   if qrycargosCobradorFOLIO_PAGADO.Value <> '' then
        begin
        qrycargosCobrador.Edit;
        qrycargosCobradorENTREGA_DEFINITIVA.Value := 'N';
        qrycargosCobrador.Post;
        qrycargosCobrador.Next;
        end
           else
           begin
           qrycargosCobrador.Edit;
           if ChkAllRecepcion.Checked then
           qrycargosCobradorENTREGA_DEFINITIVA.Value := 'S'
           else
           qrycargosCobradorENTREGA_DEFINITIVA.Value := 'N';
           qrycargosCobrador.Post;
           qrycargosCobrador.Next;
           end;
  end;
  qrycargosCobrador.First;
  qrycargosCobrador.EnableControls;
  cbCargosCobrador.Checked := qrycargosCobradorENTREGA_DEFINITIVA.Value = 'S';
end;

procedure TCobranza.chkLiquidadosClick(Sender: TObject);
begin
if not chkLiquidados.Checked then
begin
qrycargosCobrador.SQL.Text:=' select co.folio,co.fecha,(select nombre from clientes where cliente_id=(select cliente_id from doctos_cc c'+
  ' where c.docto_cc_id=co.cargo_cc_id )) as nombre_cliente,'+
  ' (select cliente_id from doctos_cc c'+
  ' where c.docto_cc_id=co.cargo_cc_id ),'+
  ' co.cargo_cc_id as docto_cc_id,'+
       ' (case when co.tipo_salida is null then ''Factura'''+
             ' when co.tipo_salida = ''F'' then ''Factura'''+
             ' when co.tipo_salida = ''C'' then ''Contra recibo'''+
       ' end) as tipo_SALIDA,co.fechahora_salida,co.cobrador_id,'+
       ' cr.nombre as cobrador,co.fechahora_entrega,co.tipo_entrega as tipo_entrega,'+
       ' co.aclaraciones as aclaraciones,current_date as prox_visita,'+
       ' ''N'' as entrega_definitiva,co.folio_pagado,'+
       ' (select (importe+impuesto) as saldo_cargo from importes_doctos_cc c'+
  ' where c.docto_cc_id=co.cargo_cc_id ),(SELECT SALDO_CARGO FROM SALDO_CARGO_CC_S(co.cargo_cc_id, ''31-DEC-9999'', 0, ''S'', NULL)) as saldo_cargo1'+
' from sic_cobranza co left join'+
' cobradores cr on cr.cobrador_id = co.cobrador_id'+
' where co.cobrador_id = :cobrador_id and co.folio_pagado is null and (SELECT SALDO_CARGO FROM SALDO_CARGO_CC_S(co.cargo_cc_id, ''31-DEC-9999'', 0, ''S'', NULL))>0'+
 ' order by co.fechahora_salida';
qrycargosCobrador.Open();
grdCargoscobrador.Refresh;
end
else
begin
qrycargosCobrador.SQL.Text:=' select co.folio,co.fecha,(select nombre from clientes where cliente_id=(select cliente_id from doctos_cc c'+
  ' where c.docto_cc_id=co.cargo_cc_id )) as nombre_cliente,'+
  ' (select cliente_id from doctos_cc c'+
  ' where c.docto_cc_id=co.cargo_cc_id ),'+
  ' co.cargo_cc_id as docto_cc_id,'+
       ' (case when co.tipo_salida is null then ''Factura'''+
             ' when co.tipo_salida = ''F'' then ''Factura'''+
             ' when co.tipo_salida = ''C'' then ''Contra recibo'''+
       ' end) as tipo_SALIDA,co.fechahora_salida,co.cobrador_id,'+
       ' cr.nombre as cobrador,co.fechahora_entrega,co.tipo_entrega as tipo_entrega,'+
       ' co.aclaraciones as aclaraciones,current_date as prox_visita,'+
       ' ''N'' as entrega_definitiva,co.folio_pagado,'+
       ' (select (importe+impuesto) as saldo_cargo from importes_doctos_cc c'+
  ' where c.docto_cc_id=co.cargo_cc_id ),(SELECT SALDO_CARGO FROM SALDO_CARGO_CC_S(co.cargo_cc_id, ''31-DEC-9999'', 0, ''S'', NULL)) as saldo_cargo1'+
' from sic_cobranza co left join'+
' cobradores cr on cr.cobrador_id = co.cobrador_id'+
' where co.cobrador_id = :cobrador_id'+
 ' order by co.fechahora_salida';
qrycargosCobrador.Open();
grdCargoscobrador.Refresh;
end;
end;

procedure TCobranza.chkRemisionesClick(Sender: TObject);
begin
if chkRemisiones.Checked then
  begin
  self.Height:=self.Height+200;
  grdRemisiones.Visible:=true;
  label9.Visible:=true;
  cbAllRemisiones.Visible:=True;
  end
    else
    begin
    self.Height:=self.Height-200;
    grdRemisiones.Visible:=false;
    label9.Visible:=false;
    cbAllRemisiones.Visible:=false;
    end;
cargadatos;
end;

procedure TCobranza.ComboBox1Change(Sender: TObject);
begin
  qryCargosPendientes.Edit;
  if ComboBox1.ItemIndex = 0 then
    qryCargosPendientesTIPO_SALIDA.Value := 'F'
  else
    qryCargosPendientesTIPO_SALIDA.Value := 'C';
  qryCargosPendientes.Post;
end;

procedure TCobranza.ComboBox3Change(Sender: TObject);
begin
qryCargosCobrador.Open;
     qryCargosCobrador.Edit;

   if ComboBox3.ItemIndex = 0 then
    qrycargosCobradorTIPO_ENTREGA.Value := 'E' ;
   if ComboBox3.ItemIndex = 1 then
       qrycargosCobradorTIPO_ENTREGA.Value := 'C' ;
         if ComboBox3.ItemIndex = 2 then
       qrycargosCobradorTIPO_ENTREGA.Value := 'T' ;
        if ComboBox3.ItemIndex = 3 then
       qrycargosCobradorTIPO_ENTREGA.Value := 'R' ;
        if ComboBox3.ItemIndex = 4 then
       qrycargosCobradorTIPO_ENTREGA.Value := 'V' ;
               if ComboBox3.ItemIndex = 5 then
       qrycargosCobradorTIPO_ENTREGA.Value := '' ;
       qryCargosCobrador.Post;
       Conexion.Commit;
      // grdCargoscobrador.Refresh;
end;

procedure TCobranza.DateTimePicker1Change(Sender: TObject);
begin
              qryCargosCobrador.Edit;
       qrycargosCobradorPROX_VISITA.Value := DateTimePicker1.Date ;
       qryCargosCobrador.Post;
       Conexion.Commit;
       grdCargoscobrador.Refresh;
end;

procedure TCobranza.grdRemisionesRecepcionDrawColumnCell(Sender: TObject; const Rect: TRect;
  DataCol: Integer; Column: TColumn; State: TGridDrawState);
  const IsChecked : array[Boolean] of Integer =
 (DFCS_BUTTONCHECK, DFCS_BUTTONCHECK or DFCS_CHECKED);
var
 DrawState: Integer;
 DrawRect: TRect;
begin
 if qryRemisionesRecepcionENTREGA_DEFINITIVA.Value = 'S' then
  begin
    grdRemisionesRecepcion.Canvas.Brush.Color:=$003EC62B;
    grdRemisionesRecepcion.DefaultDrawColumnCell(Rect, DataCol, Column, State);
  end;

if Column.FieldName.ToUpper = 'ENTREGADO' then
  begin
    if (gdSelected in State) and (qryRemisionesRecepcion.RecordCount > 0) then
    begin
      with CheckBox6 do
      begin
        Left := Rect.Left + grdRemisionesRecepcion.Left + 2;
        Top := Rect.Top + grdRemisionesRecepcion.Top + 2;
        Height := Rect.Height;
        Width := Rect.Width;
        Color := grdRemisionesRecepcion.Canvas.Brush.Color;
        Visible := true;
        Checked := qryRemisionesRecepcionENTREGADO.Value = 'S';
      end;
    end
    else
    begin

      DrawRect:=Rect;
     InflateRect(DrawRect,-1,-1);

     DrawState := ISChecked[Column.Field.AsString = 'S'];
     grdRemisionesRecepcion.Canvas.FillRect(Rect);
     DrawFrameControl(grdRemisionesRecepcion.Canvas.Handle, DrawRect,
     DFC_BUTTON, DrawState);
    end;
  end;
end;

procedure TCobranza.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  conexion.Close;
  Application.Terminate;
end;

procedure TCobranza.FormCreate(Sender: TObject);
begin
Left:=0;
Top:=0;
end;

procedure TCobranza.FormShow(Sender: TObject);
begin
//SE CREA LA TABLA SIC_COBRANZA
  conexion.ExecSQL('execute block as begin if (not exists(select 1 from rdb$relations where rdb$relation_name = ''SIC_COBRANZA'')) then'+
    ' BEGIN'+
        ' execute statement ''CREATE TABLE SIC_COBRANZA ('+
                                ' CARGO_CC_ID            INTEGER,'+
                                ' FOLIO          VARCHAR(9),'+
                                ' FECHA                 DATE,'+
                                ' COBRADOR_ID                 INTEGER,'+
                                ' FECHAHORA_SALIDA               TIMESTAMP,'+
                                ' TIPO_SALIDA    CHAR(1),'+
                                ' FECHAHORA_ENTREGA  TIMESTAMP,'+
                                ' TIPO_ENTREGA        CHAR(1),'+
                                ' PROX_VISITA    DATE,'+
                                ' ACLARACIONES     VARCHAR(200),'+
                                ' ENTREGA_DEFINITIVA     CHAR(1),'+
                                ' FOLIO_PAGADO     VARCHAR(9)'+
                            ' );'';'+
    ' END end');

    //SE CREA LA TABLA SIC_COBRANZA_LOGIN
  conexion.ExecSQL('execute block as begin if (not exists(select 1 from rdb$relations where rdb$relation_name = ''SIC_COBRANZA_LOGIN'')) then'+
    ' BEGIN'+
        ' execute statement ''CREATE TABLE SIC_COBRANZA_LOGIN ('+
                                ' USUARIO          VARCHAR(9),'+
                                ' PASS          VARCHAR(20)'+
                            ' );'';'+
    ' END end');

     //SE CREA LA TABLA SIC_COBRANZA_DET
  conexion.ExecSQL('execute block as begin if (not exists(select 1 from rdb$relations where rdb$relation_name = ''SIC_COBRANZA_DET'')) then'+
    ' BEGIN'+
        ' execute statement ''CREATE TABLE SIC_COBRANZA_DET ('+
                              'ID INTEGER NOT NULL,'+
                              'CARGO_CC_ID INTEGER NOT NULL,'+
                              '"FECHA" DATE,'+
                              'FOLIO VARCHAR(11) NOT NULL'+
                               ');'';'+
        ' execute statement ''CREATE GENERATOR SIC_ID_COBRANZA;'';'+
        ' execute statement ''set GENERATOR SIC_ID_COBRANZA TO 0;'';'+
        ' execute statement ''CREATE OR ALTER TRIGGER SIC_COBRANZA_BI0 FOR SIC_COBRANZA_DET'+
                            ' ACTIVE BEFORE INSERT POSITION 0'+
                            ' AS'+
                            ' begin'+
                              ' /* genera un nuevo id */'+
                              ' if (new.id = -1) then'+
                                ' new.id = gen_id(sic_id_cobranza,1);'+
                            ' end'';'+
                                ' END end');

    //TABLA DOCTOS VE AGREGAR CAMPO COBRADOR_ID
    Conexion.ExecSQL('EXECUTE block as BEGIN if (not exists( select 1 from RDB$RELATION_FIELDS rf'+
                    ' where rf.RDB$RELATION_NAME = ''DOCTOS_VE'' and rf.RDB$FIELD_NAME = ''COBRADOR_ID'')) then'+
                    ' execute statement ''ALTER TABLE DOCTOS_VE ADD COBRADOR_ID INTEGER''; END');

     //SE CREA LA TABLA SIC_COBRANZA_PREFERENCIAS
  conexion.ExecSQL('execute block as begin if (not exists(select 1 from rdb$relations where rdb$relation_name = ''SIC_COBRANZA_PREF'')) then'+
    ' BEGIN'+
        ' execute statement ''CREATE TABLE SIC_COBRANZA_PREF ('+
                             ' CONCEPTO_CC_ID          INTEGER'+
                            ' );'';'+
    ' END end');
    Conexion.Commit;

 // cargadatos;
    qrycobradores.open;
 { qryRemisiones.Open;
  qryRemisiones1.Open;
    qryRemisionesRecepcion.Open;
  qryReportesFecha.Open;
  qryFecha.Open;
  qryCobranza.Open;
  qryCobradoresRecepcion.Open;
  qrycargosCobrador.Open;
  qryClientes.Open;
  qryVendedores.Open;   }
  PageControl1.ActivePageIndex:=0;

  dtpInicio.DateTime:=Now;
  dtpFin.DateTime:=Now;
  DateTimePicker1.DateTime:=Now;
  DateTimePicker2.DateTime:=Now;
  DateTimePicker3.DateTime:=Now;
  grdCargosSalida.SetFocus;


end;

procedure TCobranza.grdCargoscobradorDrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumn; State: TGridDrawState);
const IsChecked : array[Boolean] of Integer =
 (DFCS_BUTTONCHECK, DFCS_BUTTONCHECK or DFCS_CHECKED);
var
 DrawState: Integer;
 DrawRect: TRect;
begin
  if qrycargosCobradorFOLIO_PAGADO.Value <> '' then
  begin
    grdCargoscobrador.Canvas.Brush.Color:=$003EC62B;
    grdCargoscobrador.DefaultDrawColumnCell(Rect, DataCol, Column, State);
  end;
    if qrycargosCobradorSALDO_CARGO1.Value =0 then
  begin
    grdCargoscobrador.Canvas.Brush.Color:=$003EC62B;
    grdCargoscobrador.DefaultDrawColumnCell(Rect, DataCol, Column, State);
  end;
  if Column.FieldName.ToUpper = 'ENTREGA_DEFINITIVA' then
  begin
    if (gdSelected in State) and (qrycargosCobrador.RecordCount > 0) then
    begin
      with cbCargosCobrador do
      begin
        Left := Rect.Left + grdCargoscobrador.Left + 2;
        Top := Rect.Top + grdCargoscobrador.Top + 2;
        Height := Rect.Height;
        Width := Rect.Width;
        Color := grdCargoscobrador.Canvas.Brush.Color;
        Visible := true;
        Checked := qrycargosCobradorENTREGA_DEFINITIVA.Value = 'S';
      end;
    end
    else
    begin
      DrawRect:=Rect;
     InflateRect(DrawRect,-1,-1);
     DrawState := ISChecked[Column.Field.AsString = 'S'];
     grdCargoscobrador.Canvas.FillRect(Rect);
     DrawFrameControl(grdCargoscobrador.Canvas.Handle, DrawRect,
     DFC_BUTTON, DrawState);
    end;
  end;


   if Column.FieldName.ToUpper = 'TIPO_ENTREGA_STR' then
  begin
    if (gdSelected in State) and (qryCargosCobrador.RecordCount > 0) then
    begin
    if (qryCargosCobradorTIPO_ENTREGA.Value = 'V') then
      begin
          with DateTimePicker1 do
           begin
           Left := Rect.Left + grdCargosCobrador.Left+280;
           Top := Rect.Top + grdCargosCobrador.Top ;
           Height := Rect.Height;
           Width := Rect.Width+13;
           Color := grdCargosCobrador.Canvas.Brush.Color;
           Visible := true;
           if (DateTimeToStr(qrycargosCobradorPROX_VISITA.Value) <> '') then
           DateTimePicker1.Date := qrycargosCobradorPROX_VISITA.Value;
           end;
      end;
       with ComboBox3 do
       begin
        if qryCargosCobradorTIPO_ENTREGA.Value = 'E' then
          ItemIndex := 0;

        if qryCargosCobradorTIPO_ENTREGA.Value = 'C' then
          ItemIndex := 1;

          if qryCargosCobradorTIPO_ENTREGA.Value = 'T' then
          ItemIndex := 2;

          if qryCargosCobradorTIPO_ENTREGA.Value = 'R' then
          ItemIndex := 3;

          if qryCargosCobradorTIPO_ENTREGA.Value = 'V' then
          ItemIndex := 4;

                  Left := Rect.Left + grdCargosCobrador.Left;
        Top := Rect.Top + grdCargosCobrador.Top ;
        Height := Rect.Height-2;
        Width := Rect.Width;
        Color := grdCargosCobrador.Canvas.Brush.Color;
        Visible := true;
       end;
    end;
  end;
end;


procedure TCobranza.grdCargoscobradorKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if (Key=VK_SPACE) or (Key=VK_RETURN) then
  begin
   qryCargosCobrador.Edit;
     if qrycargosCobradorENTREGA_DEFINITIVA.Value = 'S' then qrycargosCobradorENTREGA_DEFINITIVA.Value := 'N'
     else qrycargosCobradorENTREGA_DEFINITIVA.Value := 'S';
     qryCargosCobrador.Next;
     //FocusCell(grdCargoscobrador,11);
    // SimulateClick(11, 3);
    { grdCargoscobrador.Columns('TIPO_SALIDA').Drop}
  end;
end;

procedure TCobranza.grdCargosSalidaDrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumn; State: TGridDrawState);
const IsChecked : array[Boolean] of Integer =
 (DFCS_BUTTONCHECK, DFCS_BUTTONCHECK or DFCS_CHECKED);
var
 DrawState: Integer;
 DrawRect: TRect;
begin
  if qryCargosPendientesCOBRADOR.Value <> '' then
  begin
    grdCargosSalida.Canvas.Brush.Color:=$003EC62B;
    grdCargosSalida.DefaultDrawColumnCell(Rect, DataCol, Column, State);
  end;
  if Column.FieldName.ToUpper = 'ENTREGADO' then
  begin
    if (gdSelected in State) and (qryCargosPendientes.RecordCount > 0) then
    begin
      with CheckBox1 do
      begin
        Left := Rect.Left + grdCargosSalida.Left + 2;
        Top := Rect.Top + grdCargosSalida.Top + 2;
        Height := Rect.Height;
        Width := Rect.Width;
        Color := grdCargosSalida.Canvas.Brush.Color;
        Visible := true;
        Checked := qryCargosPendientesENTREGADO.Value = 'S';
      end;
    end
    else
    begin

      DrawRect:=Rect;
     InflateRect(DrawRect,-1,-1);

     DrawState := ISChecked[Column.Field.AsString = 'S'];
     grdCargosSalida.Canvas.FillRect(Rect);
     DrawFrameControl(grdCargosSalida.Canvas.Handle, DrawRect,
     DFC_BUTTON, DrawState);
    end;
  end;

  if Column.FieldName.ToUpper = 'TIPO_SALIDA_STR' then
  begin
    if (gdSelected in State) and (qryCargosPendientes.RecordCount > 0) then
    begin
      with ComboBox1 do
      begin
        Left := Rect.Left + grdCargosSalida.Left + 2;
        Top := Rect.Top + grdCargosSalida.Top ;
        Height := Rect.Height-2;
        Width := Rect.Width;
        Color := grdCargosSalida.Canvas.Brush.Color;
        Visible := true;
        if qryCargosPendientesTIPO_SALIDA.Value = 'F' then
          ItemIndex := 0
        else
          ItemIndex := 1;
      end;
    end;
  end;


end;

procedure TCobranza.grdCargosSalidaKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if (Key=VK_SPACE) or (Key=VK_RETURN) then
  begin
   qryCargosPendientes.Edit;
    if qryCargosPendientesCOBRADOR.Value = '' then
    begin
     if qryCargosPendientesENTREGADO.Value = 'S' then qryCargosPendientesENTREGADO.Value := 'N'
     else qryCargosPendientesENTREGADO.Value := 'S';
    end;
    qryCargosPendientes.Next;
  end;

end;

procedure TCobranza.grdRemisionesDrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumn; State: TGridDrawState);
  const IsChecked : array[Boolean] of Integer =
 (DFCS_BUTTONCHECK, DFCS_BUTTONCHECK or DFCS_CHECKED);
  var
   DrawState: Integer;
 DrawRect: TRect;
begin
 if qryRemisionesCOBRADOR.Value <> '' then
  begin
    grdRemisiones.Canvas.Brush.Color:=$003EC62B;
    grdRemisiones.DefaultDrawColumnCell(Rect, DataCol, Column, State);
  end;

if Column.FieldName.ToUpper = 'ENTREGADO' then
  begin
    if (gdSelected in State) and (qryRemisiones.RecordCount > 0) then
    begin
      with CheckBox3 do
      begin
        Left := Rect.Left + grdRemisiones.Left + 2;
        Top := Rect.Top + grdRemisiones.Top + 2;
        Height := Rect.Height;
        Width := Rect.Width;
        Color := grdRemisiones.Canvas.Brush.Color;
        Visible := true;
        Checked := qryRemisionesENTREGADO.Value = 'S';
      end;
    end
    else
    begin

      DrawRect:=Rect;
     InflateRect(DrawRect,-1,-1);

     DrawState := ISChecked[Column.Field.AsString = 'S'];
     grdRemisiones.Canvas.FillRect(Rect);
     DrawFrameControl(grdRemisiones.Canvas.Handle, DrawRect,
     DFC_BUTTON, DrawState);
    end;
  end;
end;

procedure TCobranza.PageControl1Change(Sender: TObject);
begin
if PageControl1.ActivePage.Caption='Recepcion de Cobranza' then
  begin
  qryCobradoresRecepcion.Open();
  qrycargosCobrador.ParamByName('cobrador_id').Value := qryCobradoresRecepcion.FieldByName('cobrador_id').AsInteger;
  qrycargosCobrador.Open();
  qrycargosCobrador.Refresh;
  end;

  if PageControl1.ActivePage.Caption='Reportes' then
  begin
  qryReportesFecha.Open();
  end;
end;

procedure TCobranza.PageControl1Enter(Sender: TObject);
begin
  qrycargosCobrador.ParamByName('cobrador_id').Value := qryCobradoresRecepcion.FieldByName('cobrador_id').AsInteger;
  qrycargosCobrador.Refresh;
end;

procedure TCobranza.qrycargosCobradorCalcFields(DataSet: TDataSet);
begin
  if qryCargosCobradorTIPO_ENTREGA.Value = 'E' then
    qryCargosCobradorTIPO_ENTREGA_STR.Value := 'Efectivo';
  if qryCargosCobradorTIPO_ENTREGA.Value = 'C' then
    qrycargosCobradorTIPO_ENTREGA_STR.Value := 'Cheque';
      if qryCargosCobradorTIPO_ENTREGA.Value = 'T' then
    qrycargosCobradorTIPO_ENTREGA_STR.Value := 'Transferencia';
      if qryCargosCobradorTIPO_ENTREGA.Value = 'R' then
    qrycargosCobradorTIPO_ENTREGA_STR.Value := 'Contra Recibo';
      if qryCargosCobradorTIPO_ENTREGA.Value = 'V' then
    qrycargosCobradorTIPO_ENTREGA_STR.Value := 'Proxima Visita';
end;

procedure TCobranza.qryCargosPendientesAfterOpen(DataSet: TDataSet);
begin
  CheckBox1.Checked := qryCargosPendientesENTREGADO.Value = 'S';
  if qryCargosPendientesTIPO_SALIDA.Value = 'F' then
    combobox1.ItemIndex := 0
  else
    combobox1.ItemIndex := 1;
end;

procedure TCobranza.qryCargosPendientesAfterRefresh(DataSet: TDataSet);
begin
  CheckBox1.Checked := qryCargosPendientesENTREGADO.Value = 'S';
end;

procedure TCobranza.qryCargosPendientesCalcFields(DataSet: TDataSet);
begin
  if qryCargosPendientesTIPO_SALIDA.Value = 'F' then
    qryCargosPendientesTIPO_SALIDA_STR.Value := 'Factura'
  else
    qryCargosPendientesTIPO_SALIDA_STR.Value := 'Contra recibo';
end;

procedure TCobranza.rgOrdenClick(Sender: TObject);
begin
  cargaDatos;
end;

procedure TCobranza.TabSheet1Show(Sender: TObject);
var existe:string;
begin
    qryConceptosCobro.Open();
  existe:=Conexion.ExecSQLScalar('select pass from sic_cobranza_login where usuario=''Admin''');
  if existe='' then
  begin
  btnNuevaContraseña.visible:=true;
  btnPass.Visible:=false;
  end
  else
  begin
  btnNuevaContraseña.visible:=false;
  btnPass.Visible:=true;
  end;

  if Conexion.ExecSQLScalar('select count(concepto_cc_id) from SIC_COBRANZA_PREF')>0 then
   begin
   cbConcepto.KeyValue:=Conexion.ExecSQLScalar('select concepto_cc_id from SIC_COBRANZA_PREF');
   end;
end;

procedure TCobranza.txtbusquedaSalidaKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if key = VK_RETURN then
    cargadatos;
end;

procedure TCobranza.FocusCell(
   const DBGrid : TDBGrid;
   const column : integer) ; {overload;}
begin
   with TStringGrid(DBGrid) do
   begin
     Col := column;
     SetFocus;
   end;
end;



type
  TMyDBGrid = class(TDBGrid);

function TCobranza.GetCellRect(ACol, ARow : Integer) : TRect;
begin
  Result := TmyDBGrid(grdCargosCobrador).CellRect(ACol, ARow);
end;

procedure TCobranza.DBGrid1MouseUp(Sender: TObject; Button: TMouseButton; Shift:
    TShiftState; X, Y: Integer);
var
  Coords : TGridCoord;
begin
  Coords := grdCargosCobrador.MouseCoord(X, Y);
  Caption := Format('Col: %d, Row: %d', [Coords.X, Coords.Y]);
end;

procedure TCobranza.cbConceptoCloseUp(Sender: TObject);
begin
if Conexion.ExecSQLScalar('select count(concepto_cc_id) from SIC_COBRANZA_PREF')=0 then
 begin
 Conexion.ExecSQL('insert into SIC_COBRANZA_PREF(concepto_cc_id) values(:v)',[cbConcepto.KeyValue]);
 end
 else
 begin
 Conexion.ExecSQL('update SIC_COBRANZA_PREF set concepto_cc_id=:cid',[cbConcepto.KeyValue]);
 end;
 Conexion.Commit;
end;

procedure TCobranza.SimulateClick(ACol, ARow : Integer);
type
  TCoords = packed record
    XPos : SmallInt;
    YPos : SmallInt;
  end;
var
  ARect : TRect;
  Coords : TCoords;
begin
  ARect := GetCellRect(ACol, ARow);
  Coords.XPos := ARect.Left;
  Coords.YPos := ARect.Top;
  grdCargosCobrador.Perform(WM_LButtonUp, 0, Integer(Coords));
end;


end.
