unit UCobranzaVerificar;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, FireDAC.Stan.Intf, FireDAC.Stan.Option,
  FireDAC.Stan.Error, FireDAC.UI.Intf, FireDAC.Phys.Intf, FireDAC.Stan.Def,
  FireDAC.Stan.Pool, FireDAC.Stan.Async, FireDAC.Phys, FireDAC.Phys.FB,
  FireDAC.Phys.FBDef, FireDAC.VCLUI.Wait, Vcl.StdCtrls, Vcl.ComCtrls, Data.DB,
  FireDAC.Comp.Client,UCobranzaDevolver;

type
  TCobranzaVerificar = class(TForm)
    Conexion: TFDConnection;
    StatusBar1: TStatusBar;
    txtContrase�a: TEdit;
    btnEntrar: TButton;
    Label1: TLabel;
    procedure btnEntrarClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  CobranzaVerificar: TCobranzaVerificar;

implementation

{$R *.dfm}

procedure TCobranzaVerificar.btnEntrarClick(Sender: TObject);
var
FormDevolver:TCobranzaDevolver;
begin
if txtContrase�a.Text=(Conexion.ExecSQLScalar('select pass from sic_cobranza_login where usuario=''Admin''')) then
  begin
  FormDevolver := TCobranzaDevolver.Create(nil);
  FormDevolver.Conexion.Params := Conexion.Params;
  FormDevolver.StatusBar1.Panels.Items[0].Text := StatusBar1.panels.items[0].text;
  FormDevolver.ShowModal;
  self.close;
  end
  else
    begin
      ShowMessage('Contrase�a incorrecta, intentar de nuevo.');
    end;
end;

end.
