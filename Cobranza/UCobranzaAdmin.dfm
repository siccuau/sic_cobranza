﻿object CobranzaAdmin: TCobranzaAdmin
  Left = 0
  Top = 0
  Caption = 'Administrador'
  ClientHeight = 102
  ClientWidth = 250
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 73
    Top = 5
    Width = 119
    Height = 16
    Caption = 'Nueva Contrase'#241'a'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object StatusBar1: TStatusBar
    Left = 0
    Top = 89
    Width = 250
    Height = 13
    Panels = <
      item
        Width = 50
      end>
    ExplicitLeft = 8
    ExplicitTop = 119
    ExplicitWidth = 294
  end
  object txtContraseña: TEdit
    Left = 32
    Top = 24
    Width = 201
    Height = 21
    PasswordChar = '*'
    TabOrder = 1
  end
  object btnGuardar: TButton
    Left = 88
    Top = 58
    Width = 75
    Height = 25
    Caption = 'Guardar'
    TabOrder = 2
    OnClick = btnGuardarClick
  end
  object Conexion: TFDConnection
    Params.Strings = (
      'User_Name=sysdba'
      'Password=masterkey'
      'Database=D:\Microsip pruebas\AD2019.FDB'
      'Protocol=TCPIP'
      'Server=192.168.0.50'
      'DriverID=FB')
    LoginPrompt = False
    Left = 32
    Top = 136
  end
end
