unit UCobranzaAdminCambiar;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, FireDAC.Stan.Intf, FireDAC.Stan.Option,
  FireDAC.Stan.Error, FireDAC.UI.Intf, FireDAC.Phys.Intf, FireDAC.Stan.Def,
  FireDAC.Stan.Pool, FireDAC.Stan.Async, FireDAC.Phys, FireDAC.Phys.FB,
  FireDAC.Phys.FBDef, FireDAC.VCLUI.Wait, Vcl.StdCtrls, Vcl.ComCtrls, Data.DB,
  FireDAC.Comp.Client;

type
  TCobranzaAdminCambiar = class(TForm)
    Conexion: TFDConnection;
    StatusBar1: TStatusBar;
    txtContrase�aAnterior: TEdit;
    btnGuardar: TButton;
    Label1: TLabel;
    txtContrase�aNueva: TEdit;
    Label2: TLabel;
    procedure btnGuardarClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  CobranzaAdminCambiar: TCobranzaAdminCambiar;

implementation

{$R *.dfm}

procedure TCobranzaAdminCambiar.btnGuardarClick(Sender: TObject);
begin
if txtContrase�aAnterior.Text=(Conexion.ExecSQLScalar('select pass from sic_cobranza_login where usuario=''Admin''')) then
  begin
  if txtContrase�aNueva.Text<>'' then
     begin
     Conexion.ExecSQL('update sic_cobranza_login set pass=:pass where usuario=''Admin''',[txtContrase�aNueva.Text]);
     ShowMessage('La contrase�a se actualizo correctamente.');
     self.close;
     end
      else
      ShowMessage('La contrase�a nueva no puede estar vacia.');
  end
  else
  ShowMessage('La contrase�a anterior no coinicide con la BD.');

end;

end.
