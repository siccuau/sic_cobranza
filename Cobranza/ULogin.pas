unit ULogin;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.ExtCtrls, Vcl.StdCtrls, registry, Uselempresa,
  Data.DB, Data.SqlExpr, Data.DBXFirebird,  FireDAC.Stan.Intf,
  FireDAC.Stan.Option, FireDAC.Stan.Error, FireDAC.UI.Intf, FireDAC.Phys.Intf,
  FireDAC.Stan.Def, FireDAC.Stan.Pool, FireDAC.Stan.Async, FireDAC.Phys,
  FireDAC.Phys.FB, FireDAC.Phys.FBDef, FireDAC.Comp.Client, UConexiones,
  Vcl.Menus, System.UITypes, Vcl.Imaging.pngimage,{WbemScripting_TLB,}ULicencia,
  FireDAC.VCLUI.Wait, IdBaseComponent, IdComponent, IdTCPConnection,
  IdTCPClient, IdDayTime, IdUDPBase, IdUDPClient, IdSNTP, system.dateUtils;




type
  TLogin = class(TForm)
    Image1: TImage;
    lblHost: TLabel;
    Label3: TLabel;
    Label1: TLabel;
    Label2: TLabel;
    txtUsername: TEdit;
    cb_conexiones: TComboBox;
    btn_aceptar: TButton;
    btn_cancelar: TButton;
    txtPassword: TEdit;
    FDConnection: TFDConnection;
    PopMenuConexion: TPopupMenu;
    MenuItemAbrir: TMenuItem;
    MenuItem1: TMenuItem;
    IdDayTime1: TIdDayTime;
    IdSNTP1: TIdSNTP;
    procedure btn_aceptarClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure btn_cancelarClick(Sender: TObject);
    procedure MenuItemAbrirClick(Sender: TObject);
    procedure MenuItem1Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure load_conexiones();
    procedure txtPasswordKeyPress(Sender: TObject; var Key: Char);
    procedure CrearClaveDemo;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Login: TLogin;

implementation

{$R *.dfm}

procedure TLogin.CrearClaveDemo;
var
    i: integer;
    DayTimeStr,fhstr, fecha_str, hora_str, clave_enc : String;
    lic : TLicencia;
begin
 DayTimeStr := IdDayTime1.DayTimeStr;
 i := Pos(' ',DayTimeStr );
 If i > 0 Then
    fhstr := Copy(DayTimeStr ,i+1,17)
 Else
    fhstr := Copy(DayTimeStr ,1,17);
  fecha_str := fhstr.Split([' '])[0];
  hora_str := fhstr.Split([' '])[1];
  fhstr := fecha_str.Split(['-'])[2]+'/'+fecha_str.Split(['-'])[1]+'/'+fecha_str.Split(['-'])[0]+' '+
           hora_str;

 lic := TLicencia.Create;
 clave_enc := lic.Encrypt(fhstr,lic.StringToword('s1c'));
 lic.SetGlobalEnvironment(PChar('sic_demo_exp'),PChar(clave_enc));
end;

function AppisExp(clave_orig : String; DayTimeStr: string) : Boolean;
Var
 i:Integer;
 fhstr, fecha_str, hora_str, fhantstr : string;
 lic : TLicencia;
 actual, original : TDateTime;
Begin
 i := Pos(' ',DayTimeStr );
 If i > 0 Then
    fhstr := Copy(DayTimeStr ,i+1,17)
 Else
   fhstr := Copy(DayTimeStr ,1,17);
  fecha_str := fhstr.Split([' '])[0];
  hora_str := fhstr.Split([' '])[1];
  fhstr := fecha_str.Split(['-'])[2]+'/'+fecha_str.Split(['-'])[1]+'/'+fecha_str.Split(['-'])[0]+' '+
           hora_str;
  actual := StrToDateTime(fhstr);

  fhantstr := lic.Decrypt(clave_orig,lic.StringToWord('s1c'));
  original := StrToDateTime(fhantstr);

  Result :=  IncDay(original,15) < incday(actual,0);
End;

procedure TLogin.btn_aceptarClick(Sender: TObject);
var
  reg:TRegistry;
  msg:String;
  FormSeleccionaEmpresa:TSeleccionaEmpresa;
  lic : TLicencia;
begin
  lic := TLicencia.Create;
  lic.SetGlobalEnvironment('ultima_conexion_sic',inttostr(cb_conexiones.ItemIndex)+';'+txtUsername.Text);
  //SE OBTIENEN LOS DATOS DE LA CONEXION
  reg := TRegistry.Create(KEY_WRITE OR KEY_WOW64_64KEY);
  reg.RootKey := HKEY_LOCAL_MACHINE;
  if (reg.KeyExists('SOFTWARE\SIC\Conexiones\'+cb_conexiones.Text)) then
  begin
    reg.OpenKeyReadOnly('SOFTWARE\SIC\Conexiones\'+cb_conexiones.Text);
    if (reg.ReadString('Servidor')= '') then
     begin
        FDConnection.Params.Values['Server'] := 'localhost';
     end
     else
     begin
        FDConnection.Params.Values['Server'] := reg.ReadString('Servidor');
     end;
    FDConnection.Params.Values['Database'] := reg.ReadString('Datos')+'System\config.fdb';
    FDConnection.Params.Values['User_Name'] := txtUserName.Text;
    FDConnection.Params.Values['Password'] := txtPassword.Text;
    try
     FDConnection.Connected := True;

     FormSeleccionaEmpresa := TSeleccionaEmpresa.Create(nil);
     if (reg.ReadString('Servidor')= '') then
     begin
        FormSeleccionaEmpresa.FDConnection.Params.Values['Server'] := 'localhost';
     end
     else
     begin
        FormSeleccionaEmpresa.FDConnection.Params.Values['Server'] := reg.ReadString('Servidor');
     end;

     FormSeleccionaEmpresa.FDConnection.Params.Values['Database'] := reg.ReadString('Datos')+'System\config.fdb';
     FormSeleccionaEmpresa.FDConnection.Params.Values['User_Name'] := txtUserName.Text;
     FormSeleccionaEmpresa.FDConnection.Params.Values['Password'] := txtPassword.Text;
     FormSeleccionaEmpresa.FDConnection.Connected := True;
     Hide;
     FormSeleccionaEmpresa.Show;
     FDConnection.Connected := False;
    except
      msg := 'El nombre de usuario o la contrase�a no son v�lidos para el servidor de la conexion "'+cb_conexiones.Text+'".'+#13#10+'Escriba los datos correctamente o consulte al Administrador del sistema.';
      MessageDlg(msg, mtError, [mbOK], 0);
    end;
  end;
end;

procedure TLogin.load_conexiones();
var
  reg:TRegistry;
  Conexiones:TStringList;
  Conexion:String;
  FormConexion:TConexiones;
begin
    cb_conexiones.Items.Clear;
    reg := TRegistry.Create(KEY_WRITE OR KEY_WOW64_64KEY);
    reg.RootKey := HKEY_LOCAL_MACHINE;

    // AGREGA LAS CONEXIONES EXISTENTES AL COMBO DE LA PANTALLA
    if (reg.KeyExists('SOFTWARE\SIC\Conexiones')) then
    begin
      reg.OpenKeyReadOnly('SOFTWARE\SIC\Conexiones');
      if reg.HasSubKeys then
      begin
        Conexiones:=TStringList.Create;
        reg.GetKeyNames(Conexiones);
        for Conexion in Conexiones do
        begin
          cb_conexiones.AddItem(Conexion,nil);
        end;
        cb_conexiones.ItemIndex := 0;
      end
      else
      begin
        MessageDlg('No hay Conexiones SIC creadas.', mtError, [mbOK], 0);
        FormConexion:=TConexiones.Create(nil);
        FormConexion.Caption:='Nueva Conexion';
        FormConexion.ShowModal;
      end;
    end
    else
    begin
      MessageDlg('No hay Conexiones SIC creadas.', mtError, [mbOK], 0);
      FormConexion:=TConexiones.Create(nil);
      FormConexion.Caption:='Nueva Conexion';
      FormConexion.ShowModal;
    end;

end;

procedure TLogin.btn_cancelarClick(Sender: TObject);
begin
Application.Terminate;
end;

procedure TLogin.FormCreate(Sender: TObject);
var
  fechahora : TDatetime;
  fhstr, clave_orig : string;
  lic : TLicencia;
begin
  load_conexiones;

  //Demostracion 15 dias
  {lic := TLicencia.Create;
  clave_orig := lic.GetEnvVariable('sic_demo_exp');
  if clave_orig = '' then
  begin
    CrearClaveDemo;
    ShowMessage('Version Demo');
  end
  else
  begin
     if AppisExp(clave_orig,IdDayTime1.DayTimeStr) then
     begin
        ShowMessage('Demo Terminado');
        Application.Terminate;
     end
     else
     begin
        ShowMessage('Version Demo');
     end;
  end;}

  
end;

procedure TLogin.FormShow(Sender: TObject);
var
  lic : TLicencia;
  conexionIndex : integer;
  conexionUsuario:string;
begin
  lic := Tlicencia.Create;
  try
     conexionIndex := strtoint(lic.GetEnvVariable('ultima_conexion_sic').split([';'])[0]);
     conexionUsuario := UpperCase(lic.GetEnvVariable('ultima_conexion_sic').split([';'])[1]);
  Except
     conexionIndex := 0;
     conexionUsuario := 'SYSDBA'
  end;

  load_conexiones;
  cb_conexiones.ItemIndex := conexionIndex;
  txtUsername.Text := conexionUsuario;
end;

procedure TLogin.MenuItem1Click(Sender: TObject);
var
  FormConexion:TConexiones;
begin
  FormConexion:=Tconexiones.Create(nil);
  FormConexion.Caption:='Nueva Conexion';
  if FormConexion.ShowModal = mrOK then load_conexiones;
end;

procedure TLogin.MenuItemAbrirClick(Sender: TObject);
var
  FormConexion:TConexiones;
  reg:TRegistry;
begin
  FormConexion:=Tconexiones.Create(nil);
  //SE OBTIENEN LOS DATOS DE LA CONEXION
  reg := TRegistry.Create(KEY_WRITE OR KEY_WOW64_64KEY);
  reg.RootKey := HKEY_LOCAL_MACHINE;
  if (reg.KeyExists('SOFTWARE\SIC\Conexiones\'+cb_conexiones.Text)) then
  begin
    reg.OpenKeyReadOnly('SOFTWARE\SIC\Conexiones\'+cb_conexiones.Text);
    FormConexion.txtCarpetaMSP.Text:=reg.ReadString('Datos');
    FormConexion.txtServidor.Text := reg.ReadString('Servidor');
    FormConexion.txtNombre.Text := cb_conexiones.Text;
    FormConexion.txtNombre.Enabled:=False;
    FormConexion.cb_tipo_conexion.Enabled := False;
    if (reg.ReadString('Tipo') = 'Remoto') then
    begin
      FormConexion.txtServidor.Enabled:=True;
      FormConexion.cb_tipo_conexion.ItemIndex := 1;
    end
    else
    begin
      FormConexion.txtServidor.Enabled:=False;
      FormConexion.cb_tipo_conexion.ItemIndex := 0;
    end;
  end;
  FormConexion.Caption:='Abrir Conexion';
  if FormConexion.ShowModal = mrOK then load_conexiones;
end;

procedure TLogin.txtPasswordKeyPress(Sender: TObject; var Key: Char);
begin
  if Key = #13 then  btn_aceptar.SetFocus;
end;
end.
