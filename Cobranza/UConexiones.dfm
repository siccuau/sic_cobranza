object Conexiones: TConexiones
  Left = 0
  Top = 0
  BorderIcons = [biSystemMenu, biHelp]
  BorderStyle = bsSingle
  Caption = 'Conexiones'
  ClientHeight = 138
  ClientWidth = 424
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 9
    Top = 7
    Width = 87
    Height = 13
    Caption = '&Tipo de Conexion:'
    Color = clBtnFace
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Pitch = fpVariable
    Font.Style = []
    Font.Quality = fqDraft
    ParentColor = False
    ParentFont = False
  end
  object Label2: TLabel
    Left = 9
    Top = 35
    Width = 41
    Height = 13
    Caption = '&Nombre:'
    Color = clBtnFace
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Pitch = fpVariable
    Font.Style = []
    Font.Quality = fqDraft
    ParentColor = False
    ParentFont = False
  end
  object Label3: TLabel
    Left = 9
    Top = 63
    Width = 44
    Height = 13
    Caption = '&Servidor:'
    Color = clBtnFace
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Pitch = fpVariable
    Font.Style = []
    Font.Quality = fqDraft
    ParentColor = False
    ParentFont = False
  end
  object Label5: TLabel
    Left = 9
    Top = 92
    Width = 89
    Height = 13
    Caption = '&Carpeta de Datos:'
    Color = clBtnFace
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Pitch = fpVariable
    Font.Style = []
    Font.Quality = fqDraft
    ParentColor = False
    ParentFont = False
  end
  object txtNombre: TEdit
    Left = 107
    Top = 31
    Width = 210
    Height = 21
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Pitch = fpVariable
    Font.Style = []
    Font.Quality = fqDraft
    ParentFont = False
    TabOrder = 0
  end
  object txtServidor: TEdit
    Left = 107
    Top = 59
    Width = 210
    Height = 21
    Enabled = False
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Pitch = fpVariable
    Font.Style = []
    Font.Quality = fqDraft
    ParentFont = False
    TabOrder = 1
  end
  object txtCarpetaMSP: TEdit
    Left = 107
    Top = 88
    Width = 210
    Height = 21
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Pitch = fpVariable
    Font.Style = []
    Font.Quality = fqDraft
    ParentFont = False
    TabOrder = 2
  end
  object cb_tipo_conexion: TComboBox
    Left = 107
    Top = 3
    Width = 210
    Height = 21
    Style = csDropDownList
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Pitch = fpVariable
    Font.Style = []
    Font.Quality = fqDraft
    ItemIndex = 0
    ParentFont = False
    TabOrder = 3
    Text = 'Local'
    OnChange = cb_tipo_conexionChange
    Items.Strings = (
      'Local'
      'Remoto')
  end
  object btnAceptar: TButton
    Left = 336
    Top = 2
    Width = 75
    Height = 23
    Caption = 'Aceptar'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Pitch = fpVariable
    Font.Style = []
    Font.Quality = fqDraft
    ParentFont = False
    TabOrder = 4
    OnClick = btnAceptarClick
  end
  object Button2: TButton
    Left = 336
    Top = 30
    Width = 75
    Height = 23
    Caption = 'Cancelar'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Pitch = fpVariable
    Font.Style = []
    Font.Quality = fqDraft
    ParentFont = False
    TabOrder = 5
    OnClick = Button2Click
  end
  object btnEliminar: TButton
    Left = 336
    Top = 58
    Width = 75
    Height = 23
    Caption = 'Eliminar...'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Pitch = fpVariable
    Font.Style = []
    Font.Quality = fqDraft
    ParentFont = False
    TabOrder = 6
    OnClick = btnEliminarClick
  end
end
