program Cobranza;

uses
  Vcl.Forms,
  System.SysUtils,
  UCobranza in 'UCobranza.pas' {Cobranza},
  ULicenciaProv in 'ULicenciaProv.pas' {FormLicenciaProv},
  ULogin in 'ULogin.pas' {Login},
  Uselempresa in 'Uselempresa.pas' {SeleccionaEmpresa},
  UConexiones in 'UConexiones.pas' {Conexiones},
  ULicencia in 'ULicencia.pas',
  UEnvVars in 'UEnvVars.pas',
  UCobranzaDevolver in 'UCobranzaDevolver.pas' {CobranzaDevolver},
  UCobranzaAdmin in 'UCobranzaAdmin.pas' {CobranzaAdmin},
  UCobranzaAdminCambiar in 'UCobranzaAdminCambiar.pas' {CobranzaAdminCambiar},
  UCobranzaVerificar in 'UCobranzaVerificar.pas' {CobranzaVerificar},
  UVerificaLicencia in 'UVerificaLicencia.pas' {VerificaLicencia},
  WbemScripting_TLB in 'WbemScripting_TLB.pas';

{$R *.res}
  var
    obj : TLicencia;
    envvar,desenc, desenc1, desenc2 : string;

begin
  Application.Initialize;
  Application.MainFormOnTaskbar := True;
   //**********************VERIFICACION DE LICENCIA******************************
  obj := TLicencia.create;
  envvar := obj.GetEnvVariable('sic_apps_pwd');
  if envvar <> '' then
  begin
    desenc := obj.Decrypt(envvar,obj.StringToWord('@dm1nS1C'));
    desenc1 := desenc.Split([';'])[0];
    desenc2 := desenc.Split([';'])[1];
    if((desenc2 = trim(obj.GetMotherBoardSerial)) and  (desenc1 = trim(obj.GetMotherBoardSerial))) then
    begin
  Application.CreateForm(TLogin, Login);
  Application.CreateForm(TConexiones, Conexiones);
  Application.CreateForm(TCobranzaDevolver, CobranzaDevolver);
  Application.CreateForm(TCobranzaAdmin, CobranzaAdmin);
  Application.CreateForm(TCobranzaAdminCambiar, CobranzaAdminCambiar);
  Application.CreateForm(TCobranzaVerificar, CobranzaVerificar);
  Application.CreateForm(TVerificaLicencia, VerificaLicencia);
  end
    else
    begin
      desenc := obj.Decrypt(envvar,obj.StringToWord('@dm1nS1C'));
      desenc1 := desenc.Split([';'])[0];
      desenc2 := desenc.Split([';'])[1];
      if((desenc2 = trim(obj.GetPCModel)) and  (desenc1 = trim(obj.GetPCModel))) then
      begin
      Application.CreateForm(TLogin, Login);
      end
      else
      begin
        Application.CreateForm(TVerificaLicencia, VerificaLicencia);
      end;
    end;
  end
  else
  begin
    Application.CreateForm(TVerificaLicencia, VerificaLicencia);
  end;
 { Application.CreateForm(TLogin, Login);
  Application.CreateForm(TConexiones, Conexiones);
  Application.CreateForm(TCobranzaDevolver, CobranzaDevolver);
  Application.CreateForm(TCobranzaAdmin, CobranzaAdmin);
  Application.CreateForm(TCobranzaAdminCambiar, CobranzaAdminCambiar);
  Application.CreateForm(TCobranzaVerificar, CobranzaVerificar); }
  Application.Run;
end.
