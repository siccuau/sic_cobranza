unit UCobranzaDevolver;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, FireDAC.Stan.Intf, FireDAC.Stan.Option,
  FireDAC.Stan.Error, FireDAC.UI.Intf, FireDAC.Phys.Intf, FireDAC.Stan.Def,
  FireDAC.Stan.Pool, FireDAC.Stan.Async, FireDAC.Phys, FireDAC.Phys.FB,
  FireDAC.Phys.FBDef, FireDAC.VCLUI.Wait, Data.DB, FireDAC.Comp.Client,
  Vcl.ComCtrls, FireDAC.Stan.Param, FireDAC.DatS, FireDAC.DApt.Intf,
  FireDAC.DApt, FireDAC.Comp.DataSet, Vcl.StdCtrls, Vcl.Grids, Vcl.DBGrids;

type
  TCobranzaDevolver = class(TForm)
    StatusBar1: TStatusBar;
    Conexion: TFDConnection;
    grdCargosSalida: TDBGrid;
    chkRemisiones: TCheckBox;
    grdRemisiones: TDBGrid;
    Label9: TLabel;
    btnDevolver: TButton;
    qryRemisiones: TFDQuery;
    qryRemisionesENTREGADO: TStringField;
    qryRemisionesFECHA: TDateField;
    qryRemisionesFOLIO: TStringField;
    qryRemisionesNOMBRE_CLIENTE: TStringField;
    qryRemisionesDIAS_ATRASO: TLargeintField;
    qryRemisionesIMPORTE: TBCDField;
    qryRemisionesCOBRADOR: TStringField;
    qryRemisionesDOCTO_VE_ID: TIntegerField;
    dsRemisiones: TDataSource;
    dsCargosPendientes: TDataSource;
    Label1: TLabel;
    dtpInicio: TDateTimePicker;
    Label2: TLabel;
    dtpFin: TDateTimePicker;
    Button1: TButton;
    CheckBox1: TCheckBox;
    cbAll: TCheckBox;
    qryCargosPendientes: TFDQuery;
    cbAllRemisiones: TCheckBox;
    CheckBox3: TCheckBox;
    qryCargosPendientesFOLIO: TStringField;
    qryCargosPendientesFECHA: TDateField;
    qryCargosPendientesNOMBRE_CLIENTE: TStringField;
    qryCargosPendientesCLIENTE_ID: TIntegerField;
    qryCargosPendientesDOCTO_CC_ID: TIntegerField;
    qryCargosPendientesTIPO_SALIDA: TStringField;
    qryCargosPendientesFECHAHORA_SALIDA: TSQLTimeStampField;
    qryCargosPendientesCOBRADOR_ID: TIntegerField;
    qryCargosPendientesCOBRADOR: TStringField;
    qryCargosPendientesFECHAHORA_ENTREGA: TSQLTimeStampField;
    qryCargosPendientesTIPO_ENTREGA: TStringField;
    qryCargosPendientesACLARACIONES: TStringField;
    qryCargosPendientesPROX_VISITA: TDateField;
    qryCargosPendientesENTREGA_DEFINITIVA: TStringField;
    qryCargosPendientesFOLIO_PAGADO: TStringField;
    qryCargosPendientesSALDO_CARGO: TBCDField;
    qryCargosPendientesENTREGADO: TStringField;
    procedure Button1Click(Sender: TObject);
    procedure grdCargosSalidaDrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure CheckBox1Click(Sender: TObject);
    procedure cbAllClick(Sender: TObject);
    procedure btnDevolverClick(Sender: TObject);
    procedure chkRemisionesClick(Sender: TObject);
    procedure grdRemisionesDrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure cbAllRemisionesClick(Sender: TObject);
    procedure CheckBox3Click(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  CobranzaDevolver: TCobranzaDevolver;

implementation

{$R *.dfm}

procedure TCobranzaDevolver.btnDevolverClick(Sender: TObject);
begin
 //RECORRE TODOS LOS CARGOS PENDIENTES
  qryCargosPendientes.First;
  while not qryCargosPendientes.Eof do
  begin
    //VALIDA SI ESTA SELECCIONADO
    if qryCargosPendientesENTREGADO.Value = 'S' then
    begin
    Conexion.ExecSQL('delete from sic_cobranza where cargo_cc_id=:cargo',[qryCargosPendientesDOCTO_CC_ID.Value]);
    Conexion.ExecSQL('delete from sic_cobranza_det where cargo_cc_id=:cargo',[qryCargosPendientesDOCTO_CC_ID.Value]);
    Conexion.ExecSQL('update sic_cobranza set cobrador_id=NULL where folio=:folio',[qryCargosPendientesFOLIO.Value]);
    end;
    qryCargosPendientes.Next;
  end;

    //RECORRE TODOS LAS REMISIONEs PENDIENTES
  qryRemisiones.First;
  while not qryRemisiones.Eof do
  begin
    //VALIDA SI ESTA SELECCIONADO
    if qryRemisionesENTREGADO.Value = 'S' then
    begin
         Conexion.ExecSQL('update sic_cobranza set cobrador_id=NULL,entrega_definitiva=NULL where folio=:id',[qryRemisionesFOLIO.Value]);
         Conexion.ExecSQL('update doctos_ve set cobrador_id=:cobradorid where docto_ve_id=:id',[NULL,qryRemisionesDOCTO_VE_ID.Value]);
      end;

    qryRemisiones.Next;
  end;


  Conexion.Commit;
  ShowMessage('Actualizado correctamente.');
  self.Close;
end;

procedure TCobranzaDevolver.Button1Click(Sender: TObject);
begin
        //RECARGAR DATOS QUERY CARGOSPENDIENTES
    qryCargosPendientes.SQL.Text := 'select ''S'' as entregado,co.folio,co.fecha,(select nombre from clientes where cliente_id=(select cliente_id from doctos_cc c'+
  ' where c.docto_cc_id=co.cargo_cc_id )) as nombre_cliente,'+
  '(select cliente_id from doctos_cc c'+
  ' where c.docto_cc_id=co.cargo_cc_id ),'+
  'co.cargo_cc_id as docto_cc_id,'+
       '(case when co.tipo_salida is null then ''Factura'''+
             ' when co.tipo_salida = ''F'' then ''Factura'''+
             ' when co.tipo_salida = ''C'' then ''Contra recibo'''+
       ' end) as tipo_SALIDA,co.fechahora_salida,co.cobrador_id,'+
       'cr.nombre as cobrador,co.fechahora_entrega,co.tipo_entrega as tipo_entrega,'+
       'co.aclaraciones as aclaraciones,current_date as prox_visita,'+
        '''N'' as entrega_definitiva,co.folio_pagado,'+
       '(select (importe+impuesto) as saldo_cargo from importes_doctos_cc c'+
  ' where c.docto_cc_id=co.cargo_cc_id )'+
' from sic_cobranza co left join'+
' cobradores cr on cr.cobrador_id = co.cobrador_id'+
' where co.fecha between :FECHA_INI and :FECHA_FIN and co.folio_pagado is null'+
' order by co.fechahora_salida';
  qryCargosPendientes.ParamByName('FECHA_INI').AsDateTime := dtpInicio.DateTime;
  qryCargosPendientes.ParamByName('FECHA_FIN').AsDateTime := dtpFin.DateTime;
  qryCargosPendientes.Open;


   //RECARGAR DATOS QUERY REMISIONESRECEPCION
    qryRemisiones.SQL.Text := 'select ''S'' as Entregado,dve.fecha,dve.folio,c.nombre as nombre_cliente,'+
                              'datediff(day from dve.fecha_vigencia_entrega to current_date) as dias_atraso,'+
                              'importe_neto as Importe,cr.nombre as cobrador,dve.docto_ve_id'+
                               ' from doctos_ve dve left join'+
                               ' clientes c on dve.cliente_id=c.cliente_id left join'+
                               ' cobradores cr on cr.cobrador_id = dve.cobrador_id'+
                               ' where dve.tipo_docto=''R'' and dve.estatus=''P'' and dve.fecha between :FECHA_INI and :FECHA_FIN and cr.nombre<>''''';
  qryRemisiones.ParamByName('FECHA_INI').AsDateTime := dtpInicio.DateTime;
  qryRemisiones.ParamByName('FECHA_FIN').AsDateTime := dtpFin.DateTime;
  qryRemisiones.Open;

    if chkRemisiones.Checked then
  begin
          qryRemisiones.SQL.Text := 'select ''N'' as Entregado,dve.fecha,dve.folio,c.nombre as nombre_cliente,'+
                              'datediff(day from dve.fecha_vigencia_entrega to current_date) as dias_atraso,'+
                              'importe_neto as Importe,cr.nombre as cobrador,dve.docto_ve_id'+
                               ' from doctos_ve dve left join'+
                               ' clientes c on dve.cliente_id=c.cliente_id left join'+
                               ' cobradores cr on cr.cobrador_id = dve.cobrador_id'+
                               ' where dve.tipo_docto=''R'' and dve.estatus=''P'' and dve.fecha between :FECHA_INI and :FECHA_FIN and cr.nombre<>''''';

        qryRemisiones.ParamByName('FECHA_INI').AsDateTime := dtpInicio.DateTime;
        qryRemisiones.ParamByName('FECHA_FIN').AsDateTime := dtpFin.DateTime;
        qryRemisiones.Open;
  end;
end;

procedure TCobranzaDevolver.cbAllClick(Sender: TObject);
begin
 qryCargosPendientes.DisableControls;
  qryCargosPendientes.First;
  while not qryCargosPendientes.Eof do
  begin

          if cbAll.Checked then
          begin
          qryCargosPendientes.Edit;
          qryCargosPendientesENTREGADO.Value := 'S';
          qryCargosPendientes.Post;
          qryCargosPendientes.Next;
          end
           else
           begin
            qryCargosPendientes.Edit;
            qryCargosPendientesENTREGADO.Value := 'N';
            qryCargosPendientes.Post;
            qryCargosPendientes.Next;
           end;
  end;
  qryCargosPendientes.First;
  qryCargosPendientes.EnableControls;
  CheckBox1.Checked := qryCargosPendientesENTREGADO.Value = 'S';
end;

procedure TCobranzaDevolver.cbAllRemisionesClick(Sender: TObject);
begin
 qryRemisiones.DisableControls;
  qryRemisiones.First;
  while not qryRemisiones.Eof do
  begin

          if cbAllRemisiones.Checked then
          begin
          qryRemisiones.Edit;
          qryRemisionesENTREGADO.Value := 'S';
          qryRemisiones.Post;
          qryRemisiones.Next;
          end
           else
           begin
            qryRemisiones.Edit;
            qryRemisionesENTREGADO.Value := 'N';
            qryRemisiones.Post;
            qryRemisiones.Next;
           end;

  end;
  qryRemisiones.First;
  qryRemisiones.EnableControls;
  CheckBox3.Checked := qryRemisionesENTREGADO.Value = 'S';
end;

procedure TCobranzaDevolver.CheckBox1Click(Sender: TObject);
begin
 qryCargosPendientes.Edit;
  if CheckBox1.Checked then
  begin
  qryCargosPendientesENTREGADO.Value := 'S';
  end
    else
  qryCargosPendientesENTREGADO.Value := 'N';
  qryCargosPendientes.Post;
end;

procedure TCobranzaDevolver.CheckBox3Click(Sender: TObject);
begin
  qryRemisiones.Edit;
  if CheckBox3.Checked then
  begin
  qryRemisionesENTREGADO.Value := 'S';
  end
    else
  qryRemisionesENTREGADO.Value := 'N';
  qryRemisiones.Post;
end;

procedure TCobranzaDevolver.chkRemisionesClick(Sender: TObject);
begin
if chkRemisiones.Checked then
  begin
  self.Height:=self.Height+200;
  grdRemisiones.Visible:=true;
  label9.Visible:=true;
  cbAllRemisiones.Visible:=True;
  end
    else
    begin
    self.Height:=self.Height-200;
    grdRemisiones.Visible:=false;
    label9.Visible:=false;
    cbAllRemisiones.Visible:=false;
    end;
Button1.Click;
end;

procedure TCobranzaDevolver.FormShow(Sender: TObject);
begin
  dtpInicio.DateTime:=Now;
  dtpFin.DateTime:=Now;
end;

procedure TCobranzaDevolver.grdCargosSalidaDrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumn; State: TGridDrawState);
  const IsChecked : array[Boolean] of Integer =
 (DFCS_BUTTONCHECK, DFCS_BUTTONCHECK or DFCS_CHECKED);
var
 DrawState: Integer;
 DrawRect: TRect;
begin
  if qryCargosPendientesCOBRADOR.Value <> '' then
  begin
    grdCargosSalida.Canvas.Brush.Color:=$003EC62B;
    grdCargosSalida.DefaultDrawColumnCell(Rect, DataCol, Column, State);
  end;

  if Column.FieldName.ToUpper = 'ENTREGADO' then
  begin
    if (gdSelected in State) and (qryCargosPendientes.RecordCount > 0) then
    begin
      with CheckBox1 do
      begin
        Left := Rect.Left + grdCargosSalida.Left + 2;
        Top := Rect.Top + grdCargosSalida.Top + 2;
        Height := Rect.Height;
        Width := Rect.Width;
        Color := grdCargosSalida.Canvas.Brush.Color;
        Visible := true;
        Checked := qryCargosPendientesENTREGADO.Value = 'S';
      end;
    end
    else
    begin

      DrawRect:=Rect;
     InflateRect(DrawRect,-1,-1);

     DrawState := ISChecked[Column.Field.AsString = 'S'];
     grdCargosSalida.Canvas.FillRect(Rect);
     DrawFrameControl(grdCargosSalida.Canvas.Handle, DrawRect,
     DFC_BUTTON, DrawState);
    end;
  end;
end;

procedure TCobranzaDevolver.grdRemisionesDrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumn; State: TGridDrawState);
    const IsChecked : array[Boolean] of Integer =
 (DFCS_BUTTONCHECK, DFCS_BUTTONCHECK or DFCS_CHECKED);
  var
   DrawState: Integer;
 DrawRect: TRect;
begin
if qryRemisionesCOBRADOR.Value <> '' then
  begin
    grdRemisiones.Canvas.Brush.Color:=$003EC62B;
    grdRemisiones.DefaultDrawColumnCell(Rect, DataCol, Column, State);
  end;

if Column.FieldName.ToUpper = 'ENTREGADO' then
  begin
    if (gdSelected in State) and (qryRemisiones.RecordCount > 0) then
    begin
      with CheckBox3 do
      begin
        Left := Rect.Left + grdRemisiones.Left + 2;
        Top := Rect.Top + grdRemisiones.Top + 2;
        Height := Rect.Height;
        Width := Rect.Width;
        Color := grdRemisiones.Canvas.Brush.Color;
        Visible := true;
        Checked := qryRemisionesENTREGADO.Value = 'S';
      end;
    end
    else
    begin

      DrawRect:=Rect;
     InflateRect(DrawRect,-1,-1);

     DrawState := ISChecked[Column.Field.AsString = 'S'];
     grdRemisiones.Canvas.FillRect(Rect);
     DrawFrameControl(grdRemisiones.Canvas.Handle, DrawRect,
     DFC_BUTTON, DrawState);
    end;
  end;
end;

end.
