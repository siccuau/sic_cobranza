﻿object CobranzaAdminCambiar: TCobranzaAdminCambiar
  Left = 0
  Top = 0
  Caption = 'Administrador'
  ClientHeight = 154
  ClientWidth = 248
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 57
    Top = 2
    Width = 134
    Height = 16
    Caption = 'Contrase'#241'a Anterior'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Label2: TLabel
    Left = 57
    Top = 57
    Width = 119
    Height = 16
    Caption = 'Contrase'#241'a Nueva'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object StatusBar1: TStatusBar
    Left = 0
    Top = 141
    Width = 248
    Height = 13
    Panels = <
      item
        Width = 50
      end>
  end
  object txtContraseñaAnterior: TEdit
    Left = 32
    Top = 24
    Width = 201
    Height = 21
    PasswordChar = '*'
    TabOrder = 1
  end
  object btnGuardar: TButton
    Left = 88
    Top = 106
    Width = 75
    Height = 25
    Caption = 'Guardar'
    TabOrder = 2
    OnClick = btnGuardarClick
  end
  object txtContraseñaNueva: TEdit
    Left = 32
    Top = 79
    Width = 201
    Height = 21
    PasswordChar = '*'
    TabOrder = 3
  end
  object Conexion: TFDConnection
    Params.Strings = (
      'User_Name=sysdba'
      'Password=masterkey'
      'Database=D:\Microsip pruebas\AD2019.FDB'
      'Protocol=TCPIP'
      'Server=192.168.0.50'
      'DriverID=FB')
    LoginPrompt = False
    Left = 8
    Top = 104
  end
end
